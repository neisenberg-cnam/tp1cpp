//
// Created by user on 16/09/2020.
//

#ifndef UNTITLED_EX_ONE_H
#define UNTITLED_EX_ONE_H

#include <vector>

int somme(int a, int b);
void swapInt(int &a, int &b);
void swapUInt(unsigned int &a, unsigned int &b);
void sumToThirdPtr(int *a, int *b, int *c);
void sumToThirdRef(int &a, int &b, int &c);
std::vector<unsigned int> randomTable();
void displayTab(const std::vector<unsigned int> &tab);
void sortTab(std::vector<unsigned int> &tab, bool cmp(unsigned int, unsigned int));
bool gtUInt(unsigned int a, unsigned b);
bool ltUInt(unsigned int a, unsigned int b);
int test_suite_ex_one();

#endif //UNTITLED_EX_ONE_H
