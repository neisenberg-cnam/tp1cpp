//
// Created by user on 13/10/2020.
//

#ifndef TP_POINT_H
#define TP_POINT_H

#include <cstdio>
#include <cmath>

class Point
{
public:
    float x, y;

    // Default constructor
    Point() : x(0), y(0) {}

    // Constructor with x and y
    Point(float x, float y) : x(x), y(y) {}

    // Show the information about the Point
    inline void Afficher() const { printf("x: %.2f, y: %.2f", x, y); }

    // Compute the distance from the point to another one
    double distance(const Point &p) const;

    friend bool operator==(const Point &lhs, const Point &rhs) { return (lhs.x == rhs.x && lhs.y == rhs.y); }
};

#endif //TP_POINT_H
