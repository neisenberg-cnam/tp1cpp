//
// Created by Odyn on 1/5/2021.
//

#ifndef TP5_QTGRIDDISPLAY_H
#define TP5_QTGRIDDISPLAY_H


#include <QtWidgets>
#include "../Display.h"
#include "../Game.h"

class QtGridDisplay : public QGridLayout, public Display
{
    Q_OBJECT

public:
    QtGridDisplay(QWidget * parent, Game& currentGame);

    void play() override;
    void input() override;
    void updateMessage(std::vector<std::string> _messages) override;
    void updateGrid(const std::vector<std::vector<std::string>>& _grid) override;
    void updateTokensList(const std::map<std::string, std::string>& _tokens) override;

    static const QString EMPTY_WHITE;
    static const QString EMPTY_BLACK;
    static const QString TTT_X;
    static const QString TTT_O;
    static const QString CF_YELLOW;
    static const QString CF_RED;
    static const QString C_WHITE_PAWN;
    static const QString C_BLACK_PAWN;
    static const QString C_WHITE_DAME;
    static const QString C_BLACK_DAME;

private slots:
    void elementClicked();

private:
    void clear();
    void processInputNoChoice(std::string input);
    void processInputWithChoices(std::string input);
    void changeButtonAccordingToGamePlayerOne(QPushButton *btn, bool isDame);
    void changeButtonAccordingToGamePlayerTwo(QPushButton *btn, bool isDame);

};


#endif //TP5_QTGRIDDISPLAY_H
