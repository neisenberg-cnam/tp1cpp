cmake_minimum_required(VERSION 3.10)
project(untitled1)

set(CMAKE_CXX_STANDARD 17)

add_executable(untitled1 main.cpp Point.h Rectangle.h Rectangle.cpp Point.cpp Triangle.cpp Triangle.h Circle.h util.cpp util.h)

#enable_testing()
#
#add_test(point_test pointexe )
