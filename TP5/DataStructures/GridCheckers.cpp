//
// Created by Odyn on 11/22/2020.
//

#include <iostream>
#include <algorithm>
#include "GridCheckers.h"

int numberFromLetter(char c)
{
    return static_cast<int>(c) - 97;
}

GridCheckers::GridCheckers() : Grid(10, 10) {}

std::shared_ptr<Square> GridCheckers::notationToSquare(const std::string &notation) const
{
    if (notation.empty())
    {
        return std::shared_ptr<Square>();
    }
    char letter = notation.at(0);
    int column_number = numberFromLetter(letter);
    int row_number = std::stoi(notation.substr(1));

    return getSquare(row_number - 1, column_number);
}

bool GridCheckers::pawnAtNotationBelongsTo(const std::string &notation, int player) const
{
    return notationToSquare(notation)->value == player || notationToSquare(notation)->value == player * 2;
}

int GridCheckers::notationToIndex(const std::string &notation)
{
    char letter = notation.at(0);
    int column_number = numberFromLetter(letter);
    int row_number = std::stoi(notation.substr(1));

    return row_number * 10 + column_number;
}

bool GridCheckers::pathToNotationIsEmpty(std::string notationFrom, std::string notationTo) const
{
    char letter_from = notationFrom.at(0);
    char letter_to = notationTo.at(0);

    int column_number_from = numberFromLetter(letter_from);
    int row_number_from = std::stoi(notationFrom.substr(1)) - 1;

    int column_number_to = numberFromLetter(letter_to);
    int row_number_to = std::stoi(notationTo.substr(1)) - 1;

    if (row_number_from > row_number_to)
    {
        if (column_number_from > column_number_to)
        {
            for (int i = 1; i < (row_number_from - row_number_to); i++)
            {
                if (!isEmptySquare(row_number_to + i, column_number_to + i))
                {
                    return false;
                }
            }
        }
        else
        {
            for (int i = 1; i < (row_number_from - row_number_to); i++)
            {
                if (!isEmptySquare(row_number_to + i, column_number_to - i))
                {
                    return false;
                }
            }
        }
    }
    else
    {
        if (column_number_from > column_number_to)
        {
            for (int i = 1; i < (row_number_to - row_number_from); i++)
            {
                if (!isEmptySquare(row_number_from + i, column_number_from + i))
                {
                    return false;
                }
            }
        }
        else
        {
            for (int i = 1; i < (row_number_to - row_number_from); i++)
            {
                if (!isEmptySquare(row_number_from + i, column_number_from - i))
                {
                    return false;
                }
            }
        }
    }
    return false;
}

bool GridCheckers::canNotationMoveAtNotation(const std::string &notationFrom, const std::string &notationTo,
                                             int current_player, bool isDame) const
{
    if (isDame)
    {
        return canDameAtNotationMoveAtNotation(notationFrom, notationTo, current_player);
    }
    else
    {
        return canPawnAtNotationMoveAtNotation(notationFrom, notationTo, current_player);
    }
}

bool GridCheckers::canNotationTakeAtNotation(const std::string &notationFrom, const std::string &notationTo,
                                             int current_player) const
{
    if (isPawnAtNotationADameFromPlayer(notationFrom, current_player))
    {
        return canDameAtNotationTakePawnAtNotation(notationFrom, notationTo, current_player);
    }
    else
    {
        return canPawnAtNotationTakePawnAtNotation(notationFrom, notationTo, current_player);
    }
}

std::vector<std::string>
GridCheckers::givePossibleNotationsAfterTake(const std::string &notationFrom, const std::string &notationTaken,
                                             int current_player, bool isDame) const
{
    auto possibilities = std::vector<std::string>();
    if (isDame)
    {
        bool reachedBorder = false;
        std::string currentNotationFrom = notationFrom;
        std::string currentNotationTo = notationTaken;
        while (!reachedBorder)
        {
            if (isSquareAfterPosIsEmpty(currentNotationFrom, currentNotationTo))
            {
                std::string square_after = squareAfter(currentNotationFrom, currentNotationTo);
                if (!square_after.empty())
                {
                    possibilities.push_back(square_after);
                    currentNotationFrom = currentNotationTo;
                    currentNotationTo = square_after;
                }
                bool border = currentNotationTo.at(0) == 'a' || currentNotationTo.at(0) == 'j' ||
                               stoi(currentNotationTo.substr(1)) == 1 || stoi(currentNotationTo.substr(1)) == 10;
                reachedBorder = border;
            }
            else
                reachedBorder = true;

        }
    }
    else
    {
        if (isSquareAfterPosIsEmpty(notationFrom, notationTaken))
        {
            auto s = squareAfter(notationFrom, notationTaken);
            if (!s.empty())
            {
                possibilities.push_back(s);
            }
        }
    }
    return possibilities;
}

bool GridCheckers::isPawnGoingForward(const std::string &notationFrom, const std::string &notationTo,
                                      int current_player) const
{
    int row_number_from = std::stoi(notationFrom.substr(1));
    int row_number_to = std::stoi(notationTo.substr(1));

    return (current_player != 1) ? (row_number_to > row_number_from) : (row_number_from > row_number_to);
}

bool GridCheckers::isAtRangeForPawn(const std::string &notationFrom, const std::string &notationTo, int current_player)
{
    int index_from = notationToIndex(notationFrom);
    int index_to = notationToIndex(notationTo);

    return (abs(index_from - index_to) == 9 || abs(index_from - index_to) == 11);
}

bool GridCheckers::isAtRangeForDame(const std::string &notationFrom, const std::string &notationTo) const
{
    int index_from = notationToIndex(notationFrom);
    int index_to = notationToIndex(notationTo);
    bool c1 = (abs(index_from - index_to) % 9) == 0;
    bool c2 = (abs(index_from - index_to) % 11) == 0;
    bool c3 = pathToNotationIsEmpty(notationFrom, notationTo);

    return ((abs(index_from - index_to) % 9) == 0 || (abs(index_from - index_to) % 11) == 0);
}

bool GridCheckers::isPawnAtNotationADameFromPlayer(const std::string &notation, int player) const
{
    return (notationToSquare(notation)->value == player * 2);
}

bool GridCheckers::canPawnAtNotationMoveAtNotation(const std::string &notationFrom, const std::string &notationTo,
                                                   int current_player) const
{
    return isAtRangeForPawn(notationFrom, notationTo, current_player) && notationToSquare(notationTo)->isEmpty() &&
           isPawnGoingForward(notationFrom, notationTo, current_player);
}

bool GridCheckers::canDameAtNotationMoveAtNotation(const std::string &notationFrom, const std::string &notationTo,
                                                   int current_player) const
{
    return isAtRangeForDame(notationFrom, notationTo) && notationToSquare(notationTo)->isEmpty() && pathToNotationIsEmpty(notationFrom, notationTo);
}

std::string GridCheckers::squareAfter(const std::string &notationFrom, const std::string &notationTo) const
{
    char letter_from = notationFrom.at(0);
    char letter_to = notationTo.at(0);

    int column_number_from = numberFromLetter(letter_from);
    int row_number_from = std::stoi(notationFrom.substr(1)) - 1;

    int column_number_to = numberFromLetter(letter_to);
    int row_number_to = std::stoi(notationTo.substr(1)) - 1;

    bool outOfRangeCheck = ((row_number_to + 1 <= 9)
                            && (row_number_to - 1 >= 0)
                            && (column_number_to + 1 <= 9)
                            && (column_number_to - 1 >= 0));

    if (row_number_from > row_number_to && outOfRangeCheck)
    {
        if (column_number_from > column_number_to)
        {
            return coordinatesToNotation(row_number_to - 1, column_number_to - 1);
        }
        else
        {
            return coordinatesToNotation(row_number_to - 1, column_number_to + 1);
        }
    }
    else if (outOfRangeCheck)
    {
        if (column_number_from > column_number_to)
        {
            return coordinatesToNotation(row_number_to + 1, column_number_to - 1);
        }
        else
        {
            return coordinatesToNotation(row_number_to + 1, column_number_to + 1);
        }
    }
    return "";
}

std::string GridCheckers::coordinatesToNotation(int i, int j)
{
    char a = 'a';
    std::string s;
    a += j;
    s += a;
    s += std::to_string(i + 1);
    return s;
}

bool GridCheckers::isSquareAfterPosIsEmpty(const std::string &notationFrom, const std::string &notationTo) const
{
    if (!squareAfter(notationFrom, notationTo).empty())
    {
        return notationToSquare(squareAfter(notationFrom, notationTo))->isEmpty();
    }
    else
    {
        return false;
    }
}

bool GridCheckers::isOtherPawnOpponents(const std::string &notationFrom, const std::string &notationTo,
                                        int current_player) const
{
    return (current_player != notationToSquare(notationTo)->value)
           && (current_player != notationToSquare(notationTo)->value)
           && (notationToSquare(notationTo)->value != 0);
}

bool GridCheckers::canPawnAtNotationTakePawnAtNotation(const std::string &notationFrom, const std::string &notationTo,
                                                  int current_player) const
{
    return (isOtherPawnOpponents(notationFrom, notationTo, current_player) &&
            isAtRangeForPawn(notationFrom, notationTo, current_player) &&
            isSquareAfterPosIsEmpty(notationFrom, notationTo));
}

bool GridCheckers::canDameAtNotationTakePawnAtNotation(const std::string &notationFrom, const std::string &notationTo,
                                                  int player) const
{
    return (isOtherPawnOpponents(notationFrom, notationTo, player) && isAtRangeForDame(notationFrom, notationTo) &&
            isSquareAfterPosIsEmpty(notationFrom, notationTo));
}

void GridCheckers::dameTransformation()
{
    for(auto& squ : _rows[0].getVector())
    {
        squ->value = squ->value == 1 ? 2 : squ->value;
    }
    for(auto& squ : _rows[9].getVector())
    {
        squ->value = squ->value == 3 ? 6 : squ->value;
    }
}

bool GridCheckers::isAPlayerRemovedFromGrid(int player) const
{
    for(auto& squ : _squares)
    {
        if(squ->value == player)
            return false;
    }
    return true;
}

void GridCheckers::show() const
{
    for (int k = 0; k < 10; k++)
    {
        if (k == 0)
        {
            std::cout << "  ";
        }
        char a = 'a';
        a += k;
        std::cout << "  " << a << "  ";
    }
    std::cout << std::endl;
    int h = 1;
    for (auto &row : _rows)
    {
        for (int i = 0; i < row._size; i++)
        {
            if (i == 0)
            {
                std::cout << h << (h == 10 ? "" : " ");
            }
            std::cout << "  " << row.getValueAtIndex(i) << "  ";
        }
        std::cout << std::endl;
        h++;
    }
}





