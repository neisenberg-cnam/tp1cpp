//
// Created by Odyn on 1/5/2021.
//

#include "QtGridDisplay.h"
#include "QtUtils.h"

const QString QtGridDisplay::EMPTY_WHITE =
        "background-color: white; border: 1px solid black; margin: 0; ";
const QString QtGridDisplay::EMPTY_BLACK =
        "background-color: rgb(210,180,140); border: 1px solid black; ";
const QString QtGridDisplay::TTT_X =
        "background-color: transparent; border-image: url(\"D:/dev/cours/tp1cpp/TP5/MyQtWidgets/ressources/cross.png\"); border: none; ";
const QString QtGridDisplay::TTT_O =
        "background-color: transparent; border-image: url(\"D:/dev/cours/tp1cpp/TP5/MyQtWidgets/ressources/O.png\"); border: none; ";
const QString QtGridDisplay::CF_YELLOW =
        "background-color: transparent; border-image: url(\"D:/dev/cours/tp1cpp/TP5/MyQtWidgets/ressources/yellow-dot.png\"); border: none; ";
const QString QtGridDisplay::CF_RED =
        "background-color: transparent; border-image: url(\"D:/dev/cours/tp1cpp/TP5/MyQtWidgets/ressources/red_dot.png\"); border: none; ";
const QString QtGridDisplay::C_WHITE_DAME =
        "background-color: rgb(210,180,140); border-image: url(\"D:/dev/cours/tp1cpp/TP5/MyQtWidgets/ressources/dameBlanc.png\"); border: none; ";
const QString QtGridDisplay::C_BLACK_DAME =
        "background-color: rgb(210,180,140); border-image: url(\"D:/dev/cours/tp1cpp/TP5/MyQtWidgets/ressources/dameNoir.png\"); border: none; ";
const QString QtGridDisplay::C_WHITE_PAWN =
        "background-color: rgb(210,180,140); border-image: url(\"D:/dev/cours/tp1cpp/TP5/MyQtWidgets/ressources/pionBlanc.png\"); border: none; ";
const QString QtGridDisplay::C_BLACK_PAWN =
        "background-color: rgb(210,180,140); border-image: url(\"D:/dev/cours/tp1cpp/TP5/MyQtWidgets/ressources/pionNoir.png\"); border: none; ";


QtGridDisplay::QtGridDisplay(QWidget *parent, Game &currentGame) : QGridLayout(parent), Display(currentGame)
{
    setHorizontalSpacing(0);
    setVerticalSpacing(0);
    play();
}

void QtGridDisplay::play()
{
    auto dataToDisplay = m_gameDisplayed.handleInput("begin");
    updateGrid(dataToDisplay._grid);
    updateMessage(dataToDisplay._messages);
}

void QtGridDisplay::input()
{
    // useless in case of GUI, goes through slots triggered
}

void QtGridDisplay::updateMessage(std::vector<std::string> _messages)
{
    //TODO
}

void QtGridDisplay::updateGrid(const std::vector<std::vector<std::string>>& _grid)
{
//    clear();
    int i, j = 0;
    int last_row = _grid.size();
    int last_col;
    bool flag = false;
    for (auto &line : _grid)
    {
        i++;
        j = 0;
        last_col = line.size();
        for (auto &square : line)
        {
            j++;
            if(itemAt((i-1)*line.size()+j-1) == 0)
            {
                auto btn = new QPushButton("");
                btn->setFixedSize(40,40);
                btn->setStyleSheet(QtGridDisplay::EMPTY_WHITE);
                connect(btn, &QPushButton::clicked, this, &QtGridDisplay::elementClicked);
                addWidget(btn, i, j, Qt::AlignCenter);
            }
            if(stoi(square) == m_gameDisplayed.getPlayerOneIdentifier())
            {
                QPushButton * btn = qobject_cast<QPushButton*>(itemAt((i-1)*line.size()+j-1)->widget());
                changeButtonAccordingToGamePlayerOne(btn, false);
            }
            else if(stoi(square) == m_gameDisplayed.getPlayerTwoIdentifier())
            {
                QPushButton * btn = qobject_cast<QPushButton*>(itemAt((i-1)*line.size()+j-1)->widget());
                changeButtonAccordingToGamePlayerTwo(btn, false);
            }
            else if(stoi(square) == m_gameDisplayed.getPlayerOneIdentifier() * 2)
            {
                QPushButton * btn = qobject_cast<QPushButton*>(itemAt((i-1)*line.size()+j-1)->widget());
                changeButtonAccordingToGamePlayerOne(btn, true);
            }
            else if(stoi(square) == m_gameDisplayed.getPlayerTwoIdentifier() * 2)
            {
                QPushButton * btn = qobject_cast<QPushButton*>(itemAt((i-1)*line.size()+j-1)->widget());
                changeButtonAccordingToGamePlayerTwo(btn, true);
            }
            else if(stoi(square) == 0 && m_gameDisplayed.getGameType() == "Checkers")
            {
                QPushButton * btn = qobject_cast<QPushButton*>(itemAt((i-1)*line.size()+j-1)->widget());
                if((i%2==0 && j%2!=0) || (i%2!=0 && j%2==0))
                    btn->setStyleSheet(QtGridDisplay::EMPTY_BLACK);
                else
                    btn->setStyleSheet(QtGridDisplay::EMPTY_WHITE);
            }
        }
    }
}

void QtGridDisplay::changeButtonAccordingToGamePlayerOne(QPushButton *btn, bool isDame)
{
    if (m_gameDisplayed.getGameType() == "ConnectFour")
    {
        btn->setStyleSheet(QtGridDisplay::CF_RED);
    }
    else if(m_gameDisplayed.getGameType() == "Tictactoe")
    {
        btn->setStyleSheet(QtGridDisplay::TTT_X);
    }
    else if(m_gameDisplayed.getGameType() == "Checkers")
    {
        if(isDame)
        {
            btn->setStyleSheet(QtGridDisplay::C_WHITE_DAME);
        }
        else
        {
            btn->setStyleSheet(QtGridDisplay::C_WHITE_PAWN);
        }
    }
}

void QtGridDisplay::changeButtonAccordingToGamePlayerTwo(QPushButton *btn, bool isDame)
{
    if (m_gameDisplayed.getGameType() == "ConnectFour")
    {
        btn->setStyleSheet(QtGridDisplay::CF_YELLOW);
    }
    else if(m_gameDisplayed.getGameType() == "Tictactoe")
    {
        btn->setStyleSheet(QtGridDisplay::TTT_O);
    }
    else if(m_gameDisplayed.getGameType() == "Checkers")
    {
        if(isDame)
        {
            btn->setStyleSheet(QtGridDisplay::C_BLACK_DAME);
        }
        else
        {
            btn->setStyleSheet(QtGridDisplay::C_BLACK_PAWN);
        }
    }
}

void QtGridDisplay::updateTokensList(const std::map<std::string, std::string>& _tokens)
{
    m_tokensList.clear();
    m_tokensList.insert(_tokens.begin(), _tokens.end());
}

void QtGridDisplay::clear()
{
    QLayoutItem *child;
    while ((child = takeAt(0)) != 0) {
        delete child;
    }
}

void QtGridDisplay::elementClicked()
{
    QPushButton * btnClicked = qobject_cast<QPushButton*>(sender());
    auto position = qt_utils::gridPosition(btnClicked);
    std::string input;
    if (m_gameDisplayed.getGameType() == "ConnectFour")
    {
        input = std::to_string(position.col - 1);
        processInputNoChoice(input);
    }
    else if(m_gameDisplayed.getGameType() == "Tictactoe")
    {
        input = std::to_string(position.row - 1) + "," + std::to_string(position.col - 1);
        processInputNoChoice(input);
    }
    else if(m_gameDisplayed.getGameType() == "Checkers")
    {
        char a = 'a';
        std::string s;
        a += position.col - 1;
        s += a;
        s += std::to_string(position.row);
        processInputWithChoices(s);
    }
}

void QtGridDisplay::processInputNoChoice(std::string input)
{
    auto dataToDisplay = m_gameDisplayed.handleInput(input);
    updateGrid(dataToDisplay._grid);
    if(m_gameDisplayed.getGameState() == FINISHED)
    {
        auto box = QMessageBox(this->widget());
        const std::string s = dataToDisplay._messages[0];
        QString msg = QString(s.c_str());
        box.setText(msg);
        box.exec();
    }
}

void QtGridDisplay::processInputWithChoices(std::string input)
{
    auto dataToDisplay = m_gameDisplayed.handleInput(input);
    auto dial = QMessageBox(this->widget());

    for(auto &msg : dataToDisplay._messages)
    {
        QString choice = QString(msg.c_str());
        dial.addButton(choice, QMessageBox::AcceptRole);
    }
    dial.addButton(tr("Back"), QMessageBox::RejectRole);

    dial.exec();

    auto chosen = dial.clickedButton();
    if(chosen->text() == QString("Back"))
    {
        dataToDisplay = m_gameDisplayed.handleInput("q");
    }
    else
    {
        std::string str = chosen->text().toStdString();
        dataToDisplay = m_gameDisplayed.handleInput(str.substr(0, 1));
        updateGrid(dataToDisplay._grid);
    }
}