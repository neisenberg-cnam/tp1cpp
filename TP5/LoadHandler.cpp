//
// Created by Odyn on 12/9/2020.
//

#include "LoadHandler.h"
#include <iostream>

Game *LoadHandler::LoadGameFromPath(const std::string &path)
{
    std::ifstream in;
    in.open(path, std::ios::in);
    if (in.is_open())
    {
        char type[100];
        in.getline(type, 100);
        std::string type_string = type;
        char buf[255];

        auto p1 = Player(parsePlayer(buf, in));
        auto p2 = Player(parsePlayer(buf, in));
        Game *game;
        if (type_string == "Tictactoe")
        {
            game = new Tictactoe(p1, p2);
            LoadGridGame(game, in);
        }
        else if (type_string == "ConnectFour")
        {
            game = new ConnectFour(p1, p2);
            LoadGridGame(game, in);
        }
        else if (type_string == "Checkers")
        {
            game = new Checkers(p1, p2);
            LoadGridGame(game, in);
        }
//        else if (type_string == "Othello")
//        {
//            game = new Othello(p1, p2);
//            LoadGridGame(game, in);
//        }
        else
            return nullptr;
        return game;
    }
    in.close();
    return nullptr;
}

void LoadHandler::LoadGridGame(Game *game, std::ifstream &stream)
{
    char buf[255];

    parseCurrentPlayer(game, buf, stream);
    parseGridValues(game, buf, stream);
}

int LoadHandler::parsePlayer(char *buf, std::ifstream &stream)
{
    stream.getline(buf, 255);
    int identifier = std::atoi(buf);
    *buf = '\0';
    return identifier;
}

void LoadHandler::parseCurrentPlayer(Game *game, char *buf, std::ifstream &stream)
{
    stream.getline(buf, 255);
    int current = std::atoi(buf);
    *buf = '\0';

    if (current != game->getCurrentPlayerIdentifier())
    {
        game->switchCurrentPlayer();
    }
}

void LoadHandler::parseGridValues(Game *game, char *buf, std::ifstream &stream)
{
    for (auto &row : game->getGrid().getRows())
    {
        stream.getline(buf, 255);
        std::string str_buf = buf;
        int value;
        int i = 0;
        int pos = 0;
        value = stoi(str_buf.substr(pos, 1));
        row.getSquareAtIndex(i)->value = value;
        i++;
        while (str_buf.find(';', pos) != std::string::npos)
        {
            pos = str_buf.find(';', pos) + 1;
            value = stoi(str_buf.substr(pos, 1));
            row.getSquareAtIndex(i)->value = value;
            i++;
        }
        *buf = '\0';
    }
}