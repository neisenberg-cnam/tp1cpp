#ifndef DATATODISPLAY_H
#define DATATODISPLAY_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

struct DataToDisplay
{
    std::vector<std::vector<std::string>> _grid;
    std::vector<std::string> _messages;
};

#endif // DATATODISPLAY_H
