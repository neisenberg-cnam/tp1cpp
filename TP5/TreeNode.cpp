#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "TreeNode.h"

std::map<int, std::vector<std::shared_ptr<TreeNode>>> TreeNode::m_terminalNodesGroupedByDeepness = std::map<int, std::vector<std::shared_ptr<TreeNode>>>();

TreeNode::TreeNode(const std::string _position, std::string _pathToNode, bool _isTerminal): m_isTerminal(_isTerminal), m_position(_position), m_pathToThisNode(_pathToNode)
{ 
}

void TreeNode::addChildMove(std::string _position)
{
    std::string pathToNewNode = m_pathToThisNode + "-" + _position;
    auto newNode = std::make_shared<TreeNode>(_position, pathToNewNode, true);
    m_childrenNode.push_back(newNode);
}

void TreeNode::addChildTake(const std::string& _positionOfTaken, const std::string& _position)
{
    std::string pathToNewNode = m_pathToThisNode + "x("+_positionOfTaken+")" + _position;
    auto newNode = std::make_shared<TreeNode>(_position, pathToNewNode, false);
    m_childrenNode.push_back(newNode);
}

std::string TreeNode::getPosition() const
{
    return m_position;
}

std::string TreeNode::getPathToThisNode() const
{
    return m_pathToThisNode;
}

std::vector<std::shared_ptr<TreeNode>> TreeNode::getChildrenNode() const
{
    return m_childrenNode;
}

bool TreeNode::isNodeARoot() const
{
    return m_pathToThisNode == m_position;
}

std::vector<std::shared_ptr<TreeNode>> TreeNode::findDeepestNodes(const std::shared_ptr<TreeNode>& _rootNode)
{
    if( _rootNode->m_isTerminal ){
        return std::vector<std::shared_ptr<TreeNode>>();
    }
    else {
        // on lance la complétion de m_nodesGroupedByDeepness à partir d'une node racine non-terminale
        m_terminalNodesGroupedByDeepness.clear();
        _rootNode->findTerminalNodesAmongstChildrens(0);

        // une fois tous les appels récursifs de findNextTerminalNodes effectués, et m_terminalNodesGroupedByDeepness complété, on renvoie le vector de nodes
        // avec la clé de profondeur la plus importante
        int deepest = 0;
        std::map<int, std::vector<std::shared_ptr<TreeNode>>>::iterator i;
        for(i = m_terminalNodesGroupedByDeepness.begin(); i != m_terminalNodesGroupedByDeepness.end(); i++){
            if (i->first > deepest){
                deepest = i->first;
            }
        }

        if(deepest == 1){
            bool isThereAMoveNode = false;
            bool isThereATakeNode = false;
            for(auto& node : m_terminalNodesGroupedByDeepness[deepest])
            {
                if(node->getPathToThisNode().find("-") != std::string::npos){
                    isThereAMoveNode = true;
                }
                if(node->getPathToThisNode().find("x") != std::string::npos){
                    isThereATakeNode = true;
                }
            }
            if(isThereAMoveNode && isThereATakeNode){
                for(unsigned i = 0; i < m_terminalNodesGroupedByDeepness[deepest].size(); i++){
                    if(m_terminalNodesGroupedByDeepness[deepest].at(i)->getPathToThisNode().find("-") != std::string::npos){
                        m_terminalNodesGroupedByDeepness[deepest].erase(m_terminalNodesGroupedByDeepness[deepest].begin() + i);
                    }
                }
            }
        }

        return m_terminalNodesGroupedByDeepness[deepest];
    }
}

// sert à ajouter à m_nodesGroupedByDeepness les enfants terminaux de profondeur "_currentNodeDeepness + 1" de la node qui appelle cette fonction.
void TreeNode::findTerminalNodesAmongstChildrens(int _currentNodeDeepness) const
{
    for (unsigned i = 0; i < m_childrenNode.size(); i++){
        if(m_childrenNode.at(i)->m_isTerminal){
            std::map<int, std::vector<std::shared_ptr<TreeNode>>>::iterator deepnessKey = m_terminalNodesGroupedByDeepness.find(_currentNodeDeepness + 1);

            // si un enfant est terminal, on regarde si m_nodesGroupedByDeepness contient déjà une clé de sa profondeur et on l'ajoute au vector
            // de nodes de meme profondeur
            if (deepnessKey != m_terminalNodesGroupedByDeepness.end()){
                deepnessKey->second.push_back(m_childrenNode.at(i));
            }

            // si m_nodesGroupedByDeepness ne contient pas encore d'autres nodes de cette profondeur, on ajoute un vector a la map
            else{
                std::vector<std::shared_ptr<TreeNode>> newVectorForNodesOfSameDeepness;
                newVectorForNodesOfSameDeepness.push_back(m_childrenNode.at(i));
                m_terminalNodesGroupedByDeepness[_currentNodeDeepness + 1] = newVectorForNodesOfSameDeepness;
            }
        }

        // si une node enfant n'est pas terminale, elle subit à son tour cette fonction, avec une profondeur incrémentée de 1.
        else {
            m_childrenNode.at(i)->findTerminalNodesAmongstChildrens(_currentNodeDeepness + 1);
        }
    }
}
