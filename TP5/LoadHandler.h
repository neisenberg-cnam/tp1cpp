//
// Created by Odyn on 12/9/2020.
//

#ifndef TP5_LOADHANDLER_H
#define TP5_LOADHANDLER_H


#include <string>
#include "Game.h"
#include "Checkers.h"
#include "OldGames/Tictactoe.h"
#include "OldGames/ConnectFour.h"
#include <fstream>

class LoadHandler
{
public:
    static Game* LoadGameFromPath(const std::string& path);
    static void LoadGridGame(Game* game, std::ifstream &stream);
    static int parsePlayer(char * buf, std::ifstream& stream);
    static void parseCurrentPlayer(Game* game, char * buf, std::ifstream& stream);
    static void parseGridValues(Game* game, char * buf, std::ifstream& stream);
};


#endif //TP5_LOADHANDLER_H
