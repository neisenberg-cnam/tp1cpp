//
// Created by Odyn on 10/27/2020.
//

#include "Row.h"

Row::Row() : Line()
{}

Row::Row(int size) : Line(size)
{}

Row::Row(const Row &row) : Line(row)
{}