//
// Created by Odyn on 10/27/2020.
//

#include "Tictactoe.h"
#include <iostream>
#include <regex>

Tictactoe::Tictactoe() : Game(), _grid(Grid(3,3))
{
    initGame();
}

Tictactoe::Tictactoe(const Player &p1, const Player &p2) : Game(p1, p2), _grid(Grid(3,3))
{
    initGame();
}

void Tictactoe::initGame()
{
    _currentPlayer = _p2;
}

std::vector<std::vector<std::string>> Tictactoe::getGridToDisplay()
{
    return _grid.getGridAsVectorsOfString();
}

std::string Tictactoe::processPlayerInput(const std::string& _inputValue)
{
    std::regex reg ("^[0-9],[0-9]$");

    if(std::regex_match(_inputValue, reg))
        return _inputValue;
    else
        return "";
}

bool Tictactoe::computeInputInACoin(const std::string& input)
{
    bool success;
    std::string x_str = input.substr(0, input.find(','));
    std::string y_str = input.substr(input.find(',') + 1, input.length() - 1);
    int x = std::stoi(x_str);
    int y = std::stoi(y_str);
    if(x >= _grid.width() || y >= _grid.length() )
    {
        success = false;
    }
    else
    {
        success = _grid.changeValueAt(x, y, _currentPlayer.getIdentifier());
    }

    if(success)
    {
        m_state = READYTOSWITCHCURRENTPLAYER;
    }

    return success;
}

bool Tictactoe::hasAPlayerWon() const
{
    return (_grid.isAColumnFullOfValue(_currentPlayer.getIdentifier())
            || _grid.isARowFullOfValue(_currentPlayer.getIdentifier())
            || _grid.isADiagonalFullOfValue(_currentPlayer.getIdentifier()));
}

void Tictactoe::addTextInstructionsToDataToDisplay()
{
    m_dataToDisplay._messages.push_back("Please, player " + std::to_string(_currentPlayer.getIdentifier()) +", select a square's coordinates in the format \"X,X\" in which you would like to place a token");
}
