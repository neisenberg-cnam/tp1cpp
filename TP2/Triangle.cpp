//
// Created by user on 13/10/2020.
//

#include "Triangle.h"
#include "util.h"
#include <algorithm>
#include <iostream>


std::vector<double> Triangle::lengths() const
{
    // Building a vector for every side distance for further use
    std::vector<double> vec;
    vec.push_back(_a.distance(_b));
    vec.push_back(_a.distance(_c));
    vec.push_back(_c.distance(_b));
    return vec;
}

double Triangle::base() const
{
    auto vec = lengths();
    double max = vec[0];
    // Looping on side lengths
    for (auto &len : vec)
    {
        if (max < len) { max = len; }
    }
    return max;
}


double Triangle::area() const
{
    auto vec = lengths();
    // Heron's Formula
    return 0.25 * std::sqrt(
            (vec[0] + vec[1] + vec[2]) *
            (-vec[0] + vec[1] + vec[2]) *
            (vec[0] - vec[1] + vec[2]) *
            (vec[0] + vec[1] - vec[2])
    );
}

double Triangle::height() const
{
    return 2 * area() / base();
}

bool Triangle::isIsoceles() const
{
    auto vec = lengths();
    // if 2 sides are equals, not checking equaliteral since an equilateral triangle is also isoceles
    return (floatEquals(vec[0], vec[1]) || floatEquals(vec[0], vec[2]) || floatEquals(vec[1], vec[2]));
}

bool Triangle::isEquilateral() const
{
    auto vec = lengths();
    // if A == B and B == C then A == C and all 3 are equals
    return (floatEquals(vec[0], vec[1]) && floatEquals(vec[1], vec[2]) && floatEquals(vec[0], vec[2]));
}

bool Triangle::isRectangle() const
{
    auto vec = lengths();

    // We store adjacent sides to use Pythagoria Theorem
    double adjacent1, adjacent2;
    if (floatEquals(vec[0], base()))
    {
        adjacent1 = vec[1];
        adjacent2 = vec[2];
    }
    else if (floatEquals(vec[1], base()))
    {
        adjacent1 = vec[0];
        adjacent2 = vec[2];
    }
    else
    {
        adjacent1 = vec[0];
        adjacent2 = vec[1];
    }

    return floatEquals(
            pow(base(), 2),
            (pow(adjacent1, 2) + pow(adjacent2, 2))
    );
}

void Triangle::Afficher() const
{
    _a.Afficher();
    std::cout << std::endl;
    _b.Afficher();
    std::cout << std::endl;
    _c.Afficher();
    std::cout << std::endl;
    std::cout << "Base : " << base() << std::endl
              << "Height : " << height() << std::endl
              << "Area : " << area() << std::endl
              << "Sides length : " << std::endl;
    for (const auto &len : lengths())
    {
        std::cout << len << std::endl;
    }
    std::cout << "Is isoceles ? " << (isIsoceles() ? "true" : "false") << std::endl
              << "Is Right Angled ? " << (isRectangle() ? "true" : "false") << std::endl
              << "Is Equilateral ? " << (isEquilateral() ? "true" : "false") << std::endl;
}