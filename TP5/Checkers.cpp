//
// Created by Odyn on 11/22/2020.
//

#include <iostream>
#include <regex>
#include "Checkers.h"

Checkers::Checkers() : Game()
{
    initGame();
}

Checkers::Checkers(const Player &p1, const Player &p2) : Game(p1, p2)
{
    initGame();
}

void Checkers::initGame()
{
    _p1._identifier = 1;
    _p2._identifier = 3;
    for(int i = 0; i < 4; i++)
    {
        if(i % 2 == 0)
            initGameEvenRow(i);
        else
            initGameOddRow(i);
    }
}

void Checkers::initGameOddRow(int i)
{
    for(int j = 0; j < 10; j++)
    {
        if(j % 2 != 0)
        {
            _grid.changeValueAt(i, j - 1, _p2.getIdentifier());
            _grid.changeValueAt(10-i-1, j, _p1.getIdentifier());
        }
    }
}

void Checkers::initGameEvenRow(int i)
{
    for(int j = 0; j < 10; j++)
    {
        if(j % 2 == 0)
        {
            _grid.changeValueAt(i, j + 1, _p2.getIdentifier());
            _grid.changeValueAt(10-i-1, j, _p1.getIdentifier());
        }
    }
}

std::string Checkers::processPlayerInput(const std::string& _inputValue)
{
    std::regex reg ("^[a-j]{1}[0-9]{1,2}$");

    if(std::regex_match(_inputValue, reg) && validateInput(_inputValue))
        return _inputValue;
    else
        return "";
}

bool Checkers::computeInputInACoin(const std::string& input)
{
    /*
    std::vector<std::string> choices = PawnHandler::giveChoices(input, _grid, _currentPlayer.getIdentifier());

    int i = 1;
    for(auto& choice : choices)
    {
        //std::cout << i << ". " << choice << std::endl;
        std::string out = std::to_string(i) + ". " + choice;
        notifyMessage(out);

        i++;
    }
    */

    bool success = false;

    if(m_state == AWAITINGPLACEORSELECTTOKEN){
        m_currentSelectedSquareNotation = input;
        m_currentSelectedSquareAvailableChoices = PawnHandler::giveChoices(input, _grid, _currentPlayer.getIdentifier());
        int i = 1;
        for(std::string& choice : m_currentSelectedSquareAvailableChoices)
        {
            m_dataToDisplay._messages.push_back(std::to_string(i) +" : "+choice);
            i++;
        }
        m_state = AWAITINGCHOOSEACTION;
        success = true;
    }
    else if (m_state == AWAITINGCHOOSEACTION){
        int chosen;
        try
        {
            chosen = std::stoi(input);
        }
        catch (std::exception& e)
        {}

        if(chosen > 0 && chosen <= m_currentSelectedSquareAvailableChoices.size())
        {
            PawnHandler::computeChoice(m_currentSelectedSquareAvailableChoices[chosen-1], _grid, _currentPlayer.getIdentifier());
            _grid.dameTransformation();
            success = true;
        }
        else if(input == "q")
        {
            m_state = AWAITINGPLACEORSELECTTOKEN; //pour "déselectionner" le pion courant on revient au state d'attente de sélection de pion
        }
        else
        {
            m_dataToDisplay._messages.push_back("Select an available choice or press 'q' to return to pawn selection");
        }
    }

    return success;

}


bool Checkers::validateInput(const std::string& input) const
{
    int row_number = std::stoi(input.substr(1));
    if(row_number > 10 || row_number < 1)
        return false;
    else
        return _grid.pawnAtNotationBelongsTo(input, _currentPlayer.getIdentifier());
}

bool Checkers::hasAPlayerWon() const
{
    return (_currentPlayer == _p1) ? (_grid.isAPlayerRemovedFromGrid(_p2.getIdentifier())): (_grid.isAPlayerRemovedFromGrid(_p1.getIdentifier()));
}

std::vector<std::vector<std::string>> Checkers::getGridToDisplay()
{
    return _grid.getGridAsVectorsOfString();
}

void Checkers::addTextInstructionsToDataToDisplay()
{
    m_dataToDisplay._messages.push_back("Please, player " + std::to_string(_currentPlayer.getIdentifier()) +", select a pawn's coordinates in the format \"letter,number\" which you would like to move");
}

