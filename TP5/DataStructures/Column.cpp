//
// Created by Odyn on 10/27/2020.
//

#include "Column.h"

Column::Column() : Line()
{}

Column::Column(int size) : Line(size)
{}

Column::Column(const Column &col) : Line(col)
{}