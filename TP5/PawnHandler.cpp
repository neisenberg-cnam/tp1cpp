//
// Created by Odyn on 11/22/2020.
//

#include "PawnHandler.h"

int numberFromLetterSecond(char c)
{
    return static_cast<int>(c) - 97;
}


PawnHandler::PawnHandler() {}

void PawnHandler::computeChoice(const std::string &choice, GridCheckers &grid, int player)
{
    std::size_t currentPosition = 0;
    std::string notation = "";

    auto endOfInitialPosition = choice.find("-");
    if (endOfInitialPosition == std::string::npos){
        endOfInitialPosition = choice.find("x");
    }
    grid.notationToSquare(choice.substr(0, endOfInitialPosition))->value = 0;

    while(choice.find("(", currentPosition) != std::string::npos){
        currentPosition = choice.find("(", currentPosition) + 1;
        notation = choice.substr(currentPosition, 2);
        grid.notationToSquare(notation)->value = 0;
    }
    currentPosition = choice.find_last_of(")");
    if (currentPosition != std::string::npos){
        notation = choice.substr(currentPosition + 1);
        grid.notationToSquare(notation)->value = player;
    }
    else{
        notation = choice.substr(choice.find("-") + 1);
        grid.notationToSquare(notation)->value = player;
    }
}

std::vector<std::string> PawnHandler::giveChoices(const std::string &position, GridCheckers &grid, int _player)
{
    auto _root = std::make_shared<TreeNode>(position, position, false);
    auto choices = std::vector<std::string>();
    bool isDame = grid.isPawnAtNotationADameFromPlayer(position, _player);
    createDecisionalTree(_root, grid, _player, isDame);
    for(auto& node : TreeNode::findDeepestNodes(_root))
    {
        choices.push_back(node->getPathToThisNode());
    }
    return choices;
}

void PawnHandler::createDecisionalTree(std::shared_ptr<TreeNode> _rootNode, GridCheckers &_grid, int _player, bool isDame)
{
    createChildrenFromNode(_rootNode, _grid, _player, isDame);
    for(auto & child : _rootNode->getChildrenNode()){
        if (!child->m_isTerminal)
            createDecisionalTree(child, _grid, _player, isDame);
    }
}

void PawnHandler::createChildrenFromNode(std::shared_ptr<TreeNode> _node, GridCheckers &_grid, int _player, bool isDame)
{
    char letter = _node->getPosition().at(0);
    int column_number = numberFromLetterSecond(letter);
    int row_number = std::stoi(_node->getPosition().substr(1));
    int row_number_diag_asc = row_number - column_number - 1;
    int row_number_diag_desc = row_number + column_number - 1;
    int offset_asc = 0;
    int offset_desc = 0;

    if(row_number_diag_asc < 0)
    {
        offset_asc = -row_number_diag_asc;
        row_number_diag_asc = 0;
    }
    if(row_number_diag_desc > 9)
    {
        offset_desc = row_number_diag_desc - 9;
        row_number_diag_desc = 9;
    }

    // This can be extracted in 2
    char a = 'a';
    for (int i = 0; i < 10 - row_number_diag_asc - offset_asc; i++)
    {
        std::string position;
        char ascColumnLetterOfLoop = a + i + offset_asc;
        position+= ascColumnLetterOfLoop;
        position+= std::to_string(row_number_diag_asc + i + 1);

        tryAddPositionAsMoveChildNodeToNode(_node, position, _grid, _player, isDame);
        tryAddPositionAsTakeChildNodeToNode(_node, position, _grid, _player, isDame);
    }
    for (int k = 0; k <= row_number_diag_desc - offset_desc; k++)
    {
        std::string position;
        char descColumnLetterOfLoop = a + k + offset_desc;
        position+= descColumnLetterOfLoop;
        position+= std::to_string(row_number_diag_desc - k + 1);

        tryAddPositionAsMoveChildNodeToNode(_node, position, _grid, _player, isDame);
        tryAddPositionAsTakeChildNodeToNode(_node, position, _grid, _player, isDame);
    }
    // Up to here
    if (_node->getChildrenNode().empty()){
        _node->m_isTerminal = true;
    }
}

void PawnHandler::tryAddPositionAsMoveChildNodeToNode(std::shared_ptr<TreeNode> _node, const std::string &_newPosition,
                                                      GridCheckers &_grid, int _player, bool isDame)
{
    // because only root nodes can have move children, you cannot move a pawn or dame after a take
    if (_node->isNodeARoot()){
        if (_grid.canNotationMoveAtNotation(_node->getPosition(), _newPosition, _player, isDame)){
            _node->addChildMove(_newPosition);
        }
    }
}

void
PawnHandler::tryAddPositionAsTakeChildNodeToNode(std::shared_ptr<TreeNode> _node, const std::string &_positionOfTaken,
                                                 GridCheckers &_grid, int _player, bool isDame)
{
    if (_grid.canNotationTakeAtNotation(_node->getPosition(), _positionOfTaken, _player) && _node->getPathToThisNode().find(_positionOfTaken) == std::string::npos){
        auto positionsAfterTake = _grid.givePossibleNotationsAfterTake(_node->getPosition(), _positionOfTaken, _player,
                                                                       isDame);
        for(std::string & posAfterTake : positionsAfterTake){
            _node->addChildTake(_positionOfTaken, posAfterTake);
        }
    }
}
