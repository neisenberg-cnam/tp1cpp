
#ifndef TP_RECTANGLE_H
#define TP_RECTANGLE_H

#include "Point.h"

class Rectangle
{
public:
    // Default constructor
    Rectangle() : _length(0), _width(0) {}

    // Constructor with initialization
    Rectangle(int length, int width, Point p) : _length(length), _width(width), _topLeft(p) {}

    // Accessor for length
    inline const int &length() const { return _length; }

    inline int &length() { return _length; }

    // Accessor for width
    inline const int &width() const { return _width; }
    inline int &width() { return _width; }

    // Accessor for the point in the top left corner of the rectangle
    inline const Point &topLeft() const { return _topLeft; }
    inline Point &topLeft() { return _topLeft; }

    // Returns the perimeter of the rectangle
    inline int perimeter() const { return _width * 2 + _length * 2; }

    // Returns the area of the rectangle
    inline int area() const { return _width * _length; }

    // Returns if the rectangle in parameter has a greater perimeter than the object
    inline bool hasGreaterPerimeter(const Rectangle &rect) const { return perimeter() > rect.perimeter(); }

    // Returns if the rectangle in parameter has a greater area than the object
    inline bool hasGreaterArea(const Rectangle &rect) const { return area() > rect.area(); }

    // Display all the relevant information of the rectangle
    void Afficher() const;

private:
    // Attributes
    int _length, _width;
    Point _topLeft;

};

#endif //UNTITLED1_RECTANGLE_H
