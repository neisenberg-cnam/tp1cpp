#include <iostream>
#include <assert.h>
#include "Grid.h"
#include "Tictactoe.h"
#include "ConnectFour.h"

int main()
{
    std::string in;
    do
    {
        std::cout << "Type 1 for Tictactoe 2 for ConnectFour" << std::endl;
        std::cin >> in;
    } while(std::stoi(in) != 1 && std::stoi(in) != 2);
    if(std::stoi(in) == 1)
    {
        auto game = Tictactoe();
        game.play();
    }
    else
    {
        auto game = ConnectFour();
        game.play();
    }
    return 0;
}
