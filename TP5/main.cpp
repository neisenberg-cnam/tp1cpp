#include <iostream>
#include <assert.h>
#include "DataStructures/Grid.h"
#include "OldGames/Tictactoe.h"
#include "OldGames/ConnectFour.h"
#include "Checkers.h"
#include <memory>
#include <QtWidgets/QApplication>
#include "TreeNode.h"
#include "ConsoleDisplay.h"
#include "MyQtWidgets/MainWindow.h"

QCoreApplication* createApplication(int &argc, char *argv[])
{
    for (int i = 1; i < argc; ++i) {
        if (!qstrcmp(argv[i], "-no-gui"))
            return new QCoreApplication(argc, argv);
    }
    return new QApplication(argc, argv);
}

int main(int argc, char* argv[])
{
    QScopedPointer<QCoreApplication> app(createApplication(argc, argv));

    if (qobject_cast<QApplication *>(app.data())) {
        auto win = new MainWindow();
        win->show();
    }
    else {

    }

    return app->exec();
}

void TestTree()
{
    auto root = std::make_shared<TreeNode>("imroot", "", false);

    // First level
    root->addChildMove("child_of_root1");
    root->addChildTake("i", "child_of_root2");
    root->addChildTake("i", "child_of_root3");

    // Second level
    root->getChildrenNode()[1]->addChildTake("i", "child_of_second_children");
    root->getChildrenNode()[2]->addChildTake("i", "child_of_third_children");

    // Third level
    root->getChildrenNode()[1]->getChildrenNode()[0]->addChildTake("i", "child_of_first_grandchildren");
    root->getChildrenNode()[2]->getChildrenNode()[0]->addChildTake("i", "child_of_second_grandchildren");


    root->getChildrenNode()[1]->getChildrenNode()[0]->getChildrenNode()[0]->m_isTerminal = true;
    root->getChildrenNode()[2]->getChildrenNode()[0]->getChildrenNode()[0]->m_isTerminal = true;



    assert(TreeNode::findDeepestNodes(root).size() == 2);
}
