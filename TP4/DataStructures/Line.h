//
// Created by user on 14/10/2020.
//

#ifndef TP3_LINE_H
#define TP3_LINE_H

#include <vector>
#include "Square.h"
#include <memory>

class Line
{
public:
    int _size;

    Line();
    Line(int size);
    Line(const Line &line);

    inline std::vector<std::shared_ptr<Square>> getVector() { return  _vec; }

    bool setValueAtIndex(int value, int index);
    int getValueAtIndex(int index) const;
    bool isSquareEmptyAtIndex(int index) const;
    void setSquareAtIndex(const std::shared_ptr<Square>& squ, int index);
    void pushBackSquare(const std::shared_ptr<Square>& squ);
    std::shared_ptr<Square> getSquareAtIndex(int index) const;

    bool isLineFull() const;
    bool isLineFullOfValue(int value) const;
    std::shared_ptr<Square> getLastEmptySquare() const;

private:
    std::vector<std::shared_ptr<Square>> _vec;

};

#endif //TP3_LINE_H
