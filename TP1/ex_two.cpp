//
// Created by user on 16/09/2020.
//

#include <iostream>
#include <cmath>
#include <algorithm>

enum Score {
    a = 0,
    b = 15,
    c = 30,
    d = 40,
};

Score wonTrade(Score &score)
{
    switch(score)
    {
        case a:
            return b;
        case b:
            return c;
        case c:
            return d;
        default:
            return a;
    }
}

void tennisGame(unsigned int j1, unsigned int j2)
{
    int diff = (j1 - j2);
    if ((abs(diff) > 2 && (std::min(j1, j2) > 2)) || ((std::max(j1, j2) > 4) && (std::min(j1, j2) < 2)))
    {
        std::cout << "Issue with the input, no real score possible" << std::endl;
        return;
    }
    Score j1Score = a;
    Score j2Score = a;
    for(int i = 0; i < std::min((int) j1, 3); i++)
    {
        j1Score = wonTrade(j1Score);
    }
    for(int i = 0; i < std::min((int) j2, 3); i++)
    {
        j2Score = wonTrade(j2Score);
    }
    int score;
    switch(diff)
    {
        case 2:
            score = j2Score;
            std::cout << "Joueur 1 a gagné, Joueur 2 a " << score << " points." << std::endl;
            return;
        case 1:
            score = j2Score;
            std::cout << "Joueur 1 a l'avantage, Joueur 2 a " << score << " points." << std::endl;
            return;
        case -1:
            score = j1Score;
            std::cout << "Joueur 2 a gagné, Joueur 1 a " << score << " points." << std::endl;
            return;
        case -2:
            score = j1Score;
            std::cout << "Joueur 2 a l'avantage, Joueur 1 a " << score << " points." << std::endl;
            return;
        case 0:
            std::cout << "Les deux Joueurs sont à égalité, 40-40" << std::endl;
            return;
        default:
            score = j1Score;
            int other = j2Score;
            std::cout << "Joueur 1: " << score << " - Joueur 2: " << other << std::endl;
    }
}

int test_suite_ex_two()
{
    tennisGame(4, 2);
    tennisGame(3,2);
    tennisGame(40,40);
    tennisGame(6, 1);
    tennisGame(7, 4);
}
