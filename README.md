# Projet C++ #

## Groupe: HAMMAMI Inès, BURCKERT Léa, FEITH Nicolas, EISENBERG Nicolas

## Compte-Rendu 25-11 / Choix de projet

## Relevé du projet des deux Nicolas :

__Axes d’améliorations / incompréhensions :__
* Dans la classe PawnHandler, on pourrait éventuellement appliquer un peu plus la règle Tell don’t Ask, notamment dans les if où il serait intéressant de remplacer les 9/10.
* Également dans la classe PawnHandler, il serait peut-être judicieux de faire des extractions de méthodes notamment dans compute choice, ou dans CreateChildrenFromNode.
*  Dans la classe Treenode, on pourrait également refactorer la fonction findDeepestNodes en extrayant au moins deux méthodes.
* Dans la classe Player, Player() : _identifier(std::rand() % 10) {}, essayer d’éviter les magic numbers.
* Faire attention au nombre d’indentation max ( Suivant les classes plus ou moins d’indentations (3 en moyenne, et des fois 5)).

__Axes positifs :__
* DataStructures : Dossier complet, séparation de chaque axe en classe (column, diag, line).
* Utilisation complète des constantes dans toutes les classes.
* Utilisation des vecteurs (non utilisés dans notre tp).
* Utilisation des Regex intéressantes !
* Usage d’un treenode, professionnel et efficient.
* Nommage des variables et fonctions très claires, on comprend tout de suite ce que font les fonctions et ce que signifie les variables. 


## Relevés du projet de Léa et Inès :

__Axes d’améliorations / incompréhensions :__

* Getters/Settes pour attributs privés =>  inutile => attributs publiques à la place.
* Les “const int&” références d’entier const n’ont pas vraiment de sens, autant juste avoir int (passage par copie)
* Pas de fonction avec un const terminal, un utilisateur avec le header ne sait pas quelles fonctions modifient l'objet ou non.
* Dans Dames::DeposerJeton qui semble être la seule fonction pour déplacer des pièces, vous déposez quoi qu’il arrive un Type::PION a l’emplacement d’arrivée, les dames ne peuvent donc pas persister après un déplacement.
* Des implémentations manquantes rendent le jeu de dame non-fonctionnel (prises et règles de prises)

__Axes positifs :__
* Je pense que les enums pour les couleurs/états des cases (transposable à l’id des joueurs dans notre projet) sont une bonne chose - je pense aussi

Choix à faire pour le projet gagnant : 
* CodingStyle a définir pour le groupe (français/anglais, nomenclature pour fonctions/variables)

## Modélisation succinte

Afin de créer une GUI on remplacera l’aspect grille et valeur, actuellement implémentés en console, par des éléments QT. Le but étant donc de pouvoir séparer correctement l’aspect affichage et valeurs en cours de l’aspect règle et décisions du programme.
Dans le projet des Nicolas, il y a cette écart entre `Grid` et `Game`, où Game possède des règles et Grid possède les valeurs. On peut donc facilement substituer les cases du TP3/4 en item d’un layout `QGraphicsGridLayout` et garder la gestion des règles actuelles.

L’abstraction de `Game`, présente dans les deux projets, permet elle aussi d’avoir un widget graphique qui prend simplement un jeu dans sa classe et d’accueillir la spécialisation au moment du choix du jeu, permettant d’écrire moins de code Qt.

## Décision 

Nous avons choisi de travailler avec le projet des Nicolas, leur projet étant clair et nécessitant peu de refactorisation selon les normes SOLID. Et il sera beaucoup plus facile d’ajouter une interface graphique à ce projet.

Pour aider à la compréhension de la structure du projet choisi, un diagramme de classe (VFinal) est disponible dans ce dépôt.


==================

TP4: 

Groupe: FEITH Nicolas, EISENBERG Nicolas

Nous avons décidé de garder ce projet pour les raisons suivantes : 

* Le code de Nicolas F n'avait pas été fini selon lui
* Ce code-ci utilise déjà les pointeurs intelligents et quelques abstractions pratiques.
* Outre quelques soucis de Tell don't Ask et de principe d'inversion de dépendance on peut rapidement partir sur le développement du jeu de Dame

