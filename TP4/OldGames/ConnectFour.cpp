//
// Created by Odyn on 10/27/2020.
//

#include <iostream>
#include <regex>
#include "ConnectFour.h"

ConnectFour::ConnectFour() : Game(), _grid(Grid(7,4))
{
    initGame();
}

ConnectFour::ConnectFour(const Player &p1, const Player &p2) : Game(p1, p2), _grid(Grid(7,4))
{
    initGame();
}

void ConnectFour::initGame()
{
    for(int i = 0; i < 8; i++)
    {
        auto diagonale = Diagonal(4);
        _diagonals.push_back(diagonale);
    }
    for(int i = 0; i < 4; i++)
    {
        _diagonals[i].pushBackSquare(_grid.getSquare(0, i));
        _diagonals[i].pushBackSquare(_grid.getSquare(1, 1+i));
        _diagonals[i].pushBackSquare(_grid.getSquare(2, 2+i));
        _diagonals[i].pushBackSquare(_grid.getSquare(3, 3+i));
        _diagonals[i+4].pushBackSquare(_grid.getSquare(0, 6-i));
        _diagonals[i+4].pushBackSquare(_grid.getSquare(1, 5-i));
        _diagonals[i+4].pushBackSquare(_grid.getSquare(2, 4-i));
        _diagonals[i+4].pushBackSquare(_grid.getSquare(3, 3-i));
    }
    _currentPlayer = _p2;
}

std::string ConnectFour::readPlayerInput()
{
    std::string in;
    std::cout << "Please input the column where you want to put your coin, format:\"X\" from 0 (leftmost) to 6" << std::endl;
    std::cin >> in;

    std::regex reg ("^[0-9]$");

    if(std::regex_match(in, reg))
        return in;
    else
        return "";
}

bool ConnectFour::computeInputInACoin(const std::string& input)
{
    int col_nb = std::stoi(input);
    if(col_nb >= _grid.width())
        return false;

    _grid.getLastEmptySquareFromColumn(col_nb)->value = _currentPlayer.getIdentifier();
    return true;
}

bool ConnectFour::hasAPlayerWon() const
{
    return (_grid.isAColumnFullOfValue(_currentPlayer.getIdentifier())
            || _grid.isARowFullOfValue(_currentPlayer.getIdentifier())
            || isADiagonalFullOfValue(_currentPlayer.getIdentifier()));
}

bool ConnectFour::isADiagonalFullOfValue(int value) const
{
    for(auto &dia : _diagonals)
    {
        if(dia.isLineFullOfValue(value))
            return true;
    }
    return false;
}

void ConnectFour::show()
{
    _grid.show();
}