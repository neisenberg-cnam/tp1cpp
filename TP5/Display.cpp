#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Display.h"

Display::Display(Game& _gameToDisplay): m_gameDisplayed(_gameToDisplay)
{
    m_tokensList = std::map<std::string, std::string>();
}
