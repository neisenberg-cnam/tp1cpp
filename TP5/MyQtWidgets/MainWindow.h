//
// Created by Odyn on 1/5/2021.
//

#ifndef TP5_MAINWINDOW_H
#define TP5_MAINWINDOW_H


#include <QtWidgets>
#include "../Game.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget * parent = nullptr);
    ~MainWindow() override;


private slots:
    void launchTicTacToe();
    void launchConnectFour();
    void launchCheckers();
    void saveTheGame();
    void loadAGame();
//    bool launchOthellol();


private:

    void createMenus();
    void addMenuGame();
    void addMenuSaves();

    Game * currentGame;
};


#endif //TP5_MAINWINDOW_H
