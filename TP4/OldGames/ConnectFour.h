//
// Created by Odyn on 10/27/2020.
//

#ifndef TP3_CONNECTFOUR_H
#define TP3_CONNECTFOUR_H


#include "../Player.h"
#include "../DataStructures/Grid.h"
#include "../Game.h"
#include <string>

class ConnectFour : public Game
{
public:
    ConnectFour();
    ConnectFour(const Player& p1, const Player& p2);

private:
    std::vector<Diagonal> _diagonals;

    void initGame() override;

    void show() override;
    Grid _grid;

    std::string readPlayerInput() override;

    bool computeInputInACoin(const std::string& input) override;
    bool hasAPlayerWon() const override;
    bool isADiagonalFullOfValue(int value) const;
};


#endif //TP3_CONNECTFOUR_H
