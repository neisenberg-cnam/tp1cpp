//
// Created by Odyn on 10/27/2020.
//

#ifndef TP3_COLUMN_H
#define TP3_COLUMN_H

#include "Line.h"

class Column : public Line
{
public:
    Column();
    Column(int size);
    Column(const Column& col);
};

#endif //TP3_COLUMN_H
