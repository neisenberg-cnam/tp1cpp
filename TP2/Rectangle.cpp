//
// Created by user on 13/10/2020.
//

#include <iostream>
#include "Rectangle.h"

void Rectangle::Afficher() const
{
    std::cout << "Length: " << _length << std::endl
            << "Width: " << _width << std::endl
            << "Top Left Corner: ";
    // Afficher for a point doesn't have an endline but since it's a printf we call it that way
    _topLeft.Afficher();
    std::cout << std::endl
              << "Area: " << area() << std::endl
            << "Perimeter: " << perimeter() << std::endl;
}