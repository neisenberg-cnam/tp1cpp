//
// Created by Odyn on 12/9/2020.
//
#include <iostream>
#include <fstream>

#include "SaveHandler.h"

void SaveHandler::saveGrid(const Grid &grid, const Game &game, const std::string &path)
{
    std::ofstream out;
    out.open(path, std::ios::out);
    if(out.is_open())
    {
        out << game.getGameType() << std::endl;
        out << game.getPlayerOneIdentifier() << std::endl;
        out << game.getPlayerTwoIdentifier() << std::endl;
        out << game.getCurrentPlayerIdentifier() << std::endl;
        putGridInStream(out, grid);
    }
    out.close();
}

void SaveHandler::putGridInStream(std::ofstream& stream, const Grid &grid)
{
    for(auto &row : grid.getRows())
    {
        for(auto &squ : row.getVector())
        {
            stream << squ->value;
            if(squ == row.getVector().back())
                stream << std::endl;
            else
                stream << ";";
        }
    }
}

void SaveHandler::saveCheckers(const Checkers &checkers, const std::string &path)
{
    saveGrid(checkers.getGrid(), checkers, path);
}

void SaveHandler::saveConnectFour(const ConnectFour &connectFour, const std::string &path)
{
    saveGrid(connectFour.getGrid(), connectFour, path);
}

void SaveHandler::saveTicTacToe(const Tictactoe &ttt, const std::string &path)
{
    saveGrid(ttt.getGrid(), ttt, path);
}