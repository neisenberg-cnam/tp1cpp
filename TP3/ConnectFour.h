//
// Created by Odyn on 10/27/2020.
//

#ifndef TP3_CONNECTFOUR_H
#define TP3_CONNECTFOUR_H


#include "Player.h"
#include "Grid.h"
#include "Game.h"
#include <string>

class ConnectFour : public Game
{
public:
    ConnectFour();
    ConnectFour(const Player& p1, const Player& p2);
    ~ConnectFour();

//    void play();


private:
//    Player _p1;
//    Player _p2;
//    Player _currentPlayer;
//
//    Grid _grid;
    std::vector<Diagonal> _diagonals;

    void initGame() override;

//    void playerPlayATurn();
    std::string readPlayerInput() override;
    bool computeInputInACoin(const std::string& input) override;
    bool hasAPlayerWon() const override;
    bool isADiagonalFullOfValue(int value) const;
};


#endif //TP3_CONNECTFOUR_H
