#include <iostream>
#include <assert.h>
#include "Rectangle.h"
#include "Triangle.h"

void testDistance()
{
    auto *p = new Point(0, 0);
    auto *p1 = new Point(0, 1);
    auto *p2 = new Point(1, 0);
    assert(p->distance(*p1) == 1);
    assert(p->distance(*p2) == 1);
    assert(p->distance(*p) == 0);
}

void testRectangle()
{
    auto *p = new Point(0,0);
    auto *r = new Rectangle(2, 2, *p);

    // Testing getters
    assert(r->width() == 2);
    assert(r->length() == 2);
    // Testing other methods
    assert(r->area() == 4);
    assert(r->perimeter() == 8);

    // Testing settesr
    r->length() = 3;

    assert(r->width() == 2);
    assert(r->length() == 3);
    assert(r->area() == 6);
    assert(r->perimeter() == 10);
    r->width() = 3;
    assert(r->width() == 3);
    assert(r->length() == 3);
    assert(r->area() == 9);
    assert(r->perimeter() == 12);
    r->Afficher();

    auto *r2 = new Rectangle(2,2,*p);
    assert(r->hasGreaterArea(*r2));
    assert(r->hasGreaterPerimeter(*r2));
}

void testTriangle()
{
    auto *p1 = new Point(0,2);
    auto *p2 = new Point(-2,0);
    auto *p3 = new Point(2,0);
    auto *t = new Triangle(*p1, *p2, *p3);

    assert(t->aVertex() == *p1);
    assert(t->bVertex() == *p2);
    assert(t->cVertex() == *p3);

    assert(t->base() == 4);
    assert(t->height() == 2);
    assert(t->area() == 4);

    assert(!t->isEquilateral());
    assert(t->isRectangle());
    assert(t->isIsoceles());

    t->Afficher();
}


int main()
{
    testDistance();
    testRectangle();
    testTriangle();
    return 0;
}
