#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ConsoleDisplay.h"

ConsoleDisplay::ConsoleDisplay(Game& _gameToDisplay):Display(_gameToDisplay)
{

}

void ConsoleDisplay::play()
{
    auto dataToDisplay = m_gameDisplayed.handleInput("begin");
    updateGrid(dataToDisplay._grid);
    updateMessage(dataToDisplay._messages);

    while(!(m_gameDisplayed.getGameState() == FINISHED)){
        input();
    }
}

void ConsoleDisplay::input()
{
    std::string playerInput;

    std::cin >> playerInput;

    if(playerInput.substr(0,4) == "save"){
        //TODO savegame inputValue.substr(4) (tout inputValue a partir du 4eme char) <-- dans le cas de save _inputValue == "save"+/path/
        //Prévoir donc dans la GUI un bouton save qui envoie l'input requis
        try {
            SaveHandler::saveGrid(m_gameDisplayed.getGrid(), m_gameDisplayed, playerInput.substr(4));
        } catch (std::exception e) {
//            updateMessage(e.what());
        }
    }
    else if(playerInput.substr(0,4) == "load"){
        //TODO loadgame inputValue.substr(4) (tout inputValue a partir du 4eme char) <-- dans le cas de load _inputValue == "load"+/path/
        //Prévoir donc dans la GUI un bouton save qui envoie l'input requis
        try {
            m_gameDisplayed = *(LoadHandler::LoadGameFromPath(playerInput.substr(4)));
        } catch (std::exception e) {
//            updateMessage(e.what());
        }
    }
    else
    {
        auto dataToDisplay = m_gameDisplayed.handleInput(playerInput);

        updateGrid(dataToDisplay._grid);
        updateMessage(dataToDisplay._messages);
    }

}

void ConsoleDisplay::updateMessage(std::vector<std::string> _messages)
{
    for(std::string& message : _messages)
    {
        std::cout << message << std::endl;
    }
}

void ConsoleDisplay::updateGrid(const std::vector<std::vector<std::string>>& _grid)
{
    if (m_gameDisplayed.getGameType() == "ConnectFour") {
        for (std::vector<std::string> row : _grid)
        {
            for(auto token : row)
            {
                std::cout << "  " << token << "  ";
            }
            std::cout << std::endl;
        }
    }
    else if(m_gameDisplayed.getGameType() == "Tictactoe")
    {
        for (std::vector<std::string> row : _grid)
        {
            for(auto token : row)
            {
                std::cout << "  " << token << "  ";
            }
            std::cout << std::endl;
        }
    }
    else if(m_gameDisplayed.getGameType() == "Checkers")
    {
        for (int k = 0; k < 10; k++)
        {
            if (k == 0)
            {
                std::cout << "  ";
            }
            char a = 'a';
            a += k;
            std::cout << "  " << a << "  ";
        }
        std::cout << std::endl;
        int h = 1;
        for (std::vector<std::string> row : _grid)
        {
            for (unsigned int i = 0; i < row.size(); i++)
            {
                if (i == 0)
                {
                    std::cout << h << (h == 10 ? "" : " ");
                }
                std::cout << "  " << row[i] << "  ";
            }
            std::cout << std::endl;
            h++;
        }
    }
}

void ConsoleDisplay::updateTokensList(const std::map<std::string, std::string>& _tokens)
{
    m_tokensList.clear();
    m_tokensList.insert(_tokens.begin(), _tokens.end());
}
