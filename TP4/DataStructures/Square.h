//
// Created by user on 14/10/2020.
//

#ifndef TP3_CASE_H
#define TP3_CASE_H

class Square
{
public:
    int value;

    Square();
    Square(int val);

    inline bool isEmpty() const { return value == 0; }
};

#endif //TP3_CASE_H
