//
// Created by user on 16/09/2020.
//

#include <iostream>
#include <vector>

int somme(int a, int b)
{
    return a + b;
}

void swapInt(int &a, int &b)
{
    int c = a;
    a = b;
    b = c;
}

void swapUInt(unsigned int &a, unsigned int &b)
{
    unsigned int c = a;
    a = b;
    b = c;
}

void sumToThirdPtr(int *a, int *b, int *c)
{
    *c = *a + *b;
}

void sumToThirdRef(int &a, int &b, int &c)
{
    c = a + b;
}

std::vector<unsigned int> randomTable()
{
    int input;
    std::cin >> input;

    //unsigned int tab[input];
    std::vector<unsigned int> tab;

    for(int i = 0; i < input; i++)
    {
        tab.push_back(rand() % 100);
    }

    return tab;
}

void displayTab(const std::vector<unsigned int> &tab)
{
    for(auto i : tab)
    {
        std::cout << i << " ";
    }
    std::cout << std::endl;
}

void sortTab(std::vector<unsigned int> &tab, bool cmp(unsigned int, unsigned int))
{
    bool flag = true;
    while(flag)
    {
        flag = false;
        for(int i = 1; i < tab.size(); i++)
        {
            if(cmp(tab[i-1], tab[i]))
            {
                swapUInt(tab[i-1], tab[i]);
                flag = true;
            }
        }
    }
}

bool gtUInt(unsigned int a, unsigned b)
{
    return a > b;
}

bool ltUInt(unsigned int a, unsigned int b)
{
    return a < b;
}

int test_suite_ex_one() {
    int a = 2;
    int b = 3;
    std::cout << somme(a,b) << std::endl;
    swapInt(a,b);
    std::cout << "a: " << a << " b: " << b << std::endl;
    int c = 0;
    sumToThirdPtr(&a, &b, &c);
    std::cout << "a: " << a << " b: " << b << " c: " << c << std::endl;
    c = 0;
    sumToThirdRef(a, b, c);
    std::cout << "a: " << a << " b: " << b << " c: " << c << std::endl;
    auto tab = randomTable();
    displayTab(tab);
    sortTab(tab, gtUInt);
    displayTab(tab);
    sortTab(tab, ltUInt);
    displayTab(tab);
    return 0;
}
