//
// Created by Odyn on 10/27/2020.
//

#include <iostream>
#include <regex>
#include "ConnectFour.h"

ConnectFour::ConnectFour() : Game(), _grid(Grid(7,4))
{
    initGame();
}

ConnectFour::ConnectFour(const Player &p1, const Player &p2) : Game(p1, p2), _grid(Grid(7,4))
{
    initGame();
}

void ConnectFour::initGame()
{
    for(int i = 0; i < 8; i++)
    {
        auto diagonale = Diagonal(4);
        _diagonals.push_back(diagonale);
    }
    for(int i = 0; i < 4; i++)
    {
        _diagonals[i].pushBackSquare(_grid.getSquare(0, i));
        _diagonals[i].pushBackSquare(_grid.getSquare(1, 1+i));
        _diagonals[i].pushBackSquare(_grid.getSquare(2, 2+i));
        _diagonals[i].pushBackSquare(_grid.getSquare(3, 3+i));
        _diagonals[i+4].pushBackSquare(_grid.getSquare(0, 6-i));
        _diagonals[i+4].pushBackSquare(_grid.getSquare(1, 5-i));
        _diagonals[i+4].pushBackSquare(_grid.getSquare(2, 4-i));
        _diagonals[i+4].pushBackSquare(_grid.getSquare(3, 3-i));
    }
    _currentPlayer = _p2;
}

std::vector<std::vector<std::string>> ConnectFour::getGridToDisplay()
{
    return _grid.getGridAsVectorsOfString();
}

std::string ConnectFour::processPlayerInput(const std::string& _inputValue)
{
    std::regex reg ("^[0-9]$");

    if(std::regex_match(_inputValue, reg))
        return _inputValue;
    else
        return "";
}

bool ConnectFour::computeInputInACoin(const std::string& input)
{
    int col_nb = std::stoi(input);
    if(col_nb >= _grid.width())
        return false;

    _grid.getLastEmptySquareFromColumn(col_nb)->value = _currentPlayer.getIdentifier();
    m_state = READYTOSWITCHCURRENTPLAYER;
    return true;
}

bool ConnectFour::hasAPlayerWon() const
{
    return ( (_grid.hasARowValueXTimes(_currentPlayer.getIdentifier(), 4))
            || (_grid.hasAColumnValueXTimes(_currentPlayer.getIdentifier(), 4))
            || (_grid.hasADiagonalValueXTimes(_currentPlayer.getIdentifier(), 4)) );
}


void ConnectFour::addTextInstructionsToDataToDisplay()
{
    m_dataToDisplay._messages.push_back("Please, player " + std::to_string(_currentPlayer.getIdentifier()) +", select a column number in the format \"X\" in which you would like to place a coin");
}
