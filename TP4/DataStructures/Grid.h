//
// Created by user on 14/10/2020.
//

#ifndef TP3_GRID_H
#define TP3_GRID_H

#include <vector>
#include <memory>
#include "Square.h"
#include "Row.h"
#include "Column.h"
#include "Diagonal.h"

class Grid
{
public:

    Grid();
    Grid(int width, int length);
    Grid(const Grid& grid);

    bool isEmptySquare(int i, int j) const;
    bool changeValueAt(int i, int j, int value);
    std::shared_ptr<Square> getSquare(int i, int j) const;
    std::shared_ptr<Square> getLastEmptySquareFromColumn(int col_index) const;

    bool isARowFullOfValue(int value) const;
    bool isAColumnFullOfValue(int value) const;
    bool isADiagonalFullOfValue(int value) const;

    inline int width() const
    { return _width; }
    inline int length() const
    { return _length; }

    virtual void show() const;

protected:
    int _width;
    int _length;

    std::vector<std::shared_ptr<Square>> _squares;
    std::vector<Row> _rows;
    std::vector<Column> _columns;
    std::vector<Diagonal> _diagonals;

    void initGrid();
    int computeCoordinatesInIndex(int i, int j) const;

};

#endif //TP3_GRID_H
