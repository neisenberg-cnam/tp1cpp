TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    Checkers.cpp \
    DataStructures/Column.cpp \
    DataStructures/Diagonal.cpp \
    DataStructures/Grid.cpp \
    DataStructures/GridCheckers.cpp \
    DataStructures/Line.cpp \
    DataStructures/Row.cpp \
    DataStructures/Square.cpp \
    Game.cpp \
    OldGames/ConnectFour.cpp \
    OldGames/Tictactoe.cpp \
    PawnHandler.cpp \
    TreeNode.cpp \
    main.cpp

HEADERS += \
    Checkers.h \
    DataStructures/Column.h \
    DataStructures/Diagonal.h \
    DataStructures/Grid.h \
    DataStructures/GridCheckers.h \
    DataStructures/Line.h \
    DataStructures/Row.h \
    DataStructures/Square.h \
    Game.h \
    OldGames/ConnectFour.h \
    OldGames/Tictactoe.h \
    PawnHandler.h \
    Player.h \
    TreeNode.h
