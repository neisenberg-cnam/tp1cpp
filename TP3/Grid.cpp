//
// Created by user on 14/10/2020.
//

#include <iostream>
#include "Grid.h"
#include <memory>
#include <algorithm>

int Grid::computeCoordinatesInIndex(int i, int j) const
{
    return i * _width + j;
}

void Grid::initGrid()
{
    auto squares = std::vector<Square>();
    auto rows = std::vector<Row>();
    auto columns = std::vector<Column>();
    auto diagonals = std::vector<Diagonal>();
    if (_length == _width)
    {
        auto dia_one = Diagonal(_width);
        auto dia_two = Diagonal(_width);
        _diagonals.push_back(dia_one);
        _diagonals.push_back(dia_two);
    }
    for (int k = 0; k < _width; k++)
    {
        auto col = Column(_length);
        _columns.push_back(col);

    }
    for (int h = 0; h < _length; h++)
    {
        auto row = Row(_width);
        _rows.push_back(row);
    }
    for (int i = 0; i < _length; i++)
    {
        for (int j = 0; j < _width; j++)
        {
            auto squ = std::make_shared<Square>();
            _squares.push_back(squ);
            _rows[i].pushBackSquare(squ);
            _columns[j].pushBackSquare(squ);
            if (_width == _length && i == j)
            {
                _diagonals[0].pushBackSquare(squ);
            }
            else if (_width == _length && _width - i == j)
            {
                _diagonals[1].pushBackSquare(squ);
            }
        }
    }
}

Grid::Grid() : _width(3), _length(3)
{
    initGrid();
}

Grid::Grid(int width, int length) : _width(width), _length(length)
{
    initGrid();
}

Grid::Grid(const Grid &grid) : _width(grid._width), _length(grid._length)
{
    initGrid();
}

bool Grid::isEmptySquare(int i, int j) const
{
    return _rows[i].getSquareAtIndex(j)->isEmpty();
}

bool Grid::changeValueAt(int i, int j, int value)
{
    return _rows[i].setValueAtIndex(value, j);
}

std::shared_ptr<Square> Grid::getSquare(int i, int j) const
{
    return _rows[i].getSquareAtIndex(j);
}

bool Grid::isAColumnFullOfValue(int value) const
{
    for (auto &col : _columns)
    {
        if (col.isLineFullOfValue(value))
        {
            return true;
        }
    }
    return false;

//    return std::any_of(_rows.begin(), _rows.end(), [](Row r){ return r.isLineFullOfValue(value); })
}

bool Grid::isARowFullOfValue(int value) const
{
    for (auto &row : _rows)
    {
        if (row.isLineFullOfValue(value))
        {
            return true;
        }
    }
    return false;
}

bool Grid::isADiagonalFullOfValue(int value) const
{
    for (auto &dia : _diagonals)
    {
        if (dia.isLineFullOfValue(value))
        {
            return true;
        }
    }
    return false;
}

std::shared_ptr<Square> Grid::getLastEmptySquareFromColumn(int col_index) const
{
    return _columns[col_index].getLastEmptySquare();
}

void Grid::show() const
{
    for (auto &row : _rows)
    {
        for (int i = 0; i < row._size; i++)
        {
            std::cout << "  " << row.getValueAtIndex(i) << "  ";
        }
        std::cout << std::endl;
    }
}