//
// Created by Odyn on 10/27/2020.
//

#include <iostream>
#include "Game.h"

Game::Game() : _p1(Player()), _p2(Player()), _currentPlayer(_p1), m_state(STARTINGNEWTURN)
{}

Game::Game(const Player &p1, const Player &p2) : _p1(p1), _p2(p2), _currentPlayer(_p1), m_state(STARTINGNEWTURN)
{}

void Game::switchCurrentPlayer()
{
    _currentPlayer = (_currentPlayer == _p1) ? _p2 : _p1;
}

void Game::play()
{
    /*
    bool isGameOver = false;
    while(!isGameOver)
    {
        show();
        switchCurrentPlayer();
        currentPlayerPlayATurn();
        if(hasAPlayerWon())
        {
            //std::cout << "Congratulations Player " << ((_currentPlayer == _p1) ? "1" : "2") << ", you won !" << std::endl;
            std::string winMessage = (std::string)"Congratulations Player " + ((_currentPlayer == _p1) ? "1" : "2") + ", you won !";
            notifyMessage(winMessage);
            isGameOver = true;
            show();
        }
        std::cout << "______________" << std::endl;
    }
    */
}

void Game::currentPlayerPlayATurn()
{
    /*
    bool valid = false;
    while(!valid)
    {
        std::string raw_input = readPlayerInput();
        if(!raw_input.empty())
        {
            valid = computeInputInACoin(raw_input);
        }
    }
    */
}

GameState Game::getGameState() const
{
    return m_state;
}

DataToDisplay& Game::handleInput(std::string _inputValue)
{
    m_dataToDisplay._grid.clear();
    m_dataToDisplay._messages.clear();

    if(_inputValue == "begin")
    {
        m_dataToDisplay._messages.push_back("Started a game of "+getGameType()+" !");
    }

    if(m_state == AWAITINGPLACEORSELECTTOKEN){
        if(!processPlayerInput(_inputValue).empty()){
            computeInputInACoin(_inputValue);
        }
    }
    else if(m_state == AWAITINGCHOOSEACTION){ //optionnel, certains jeux ne passent jamais par l'état AWAITINGCHOOSEACTION
        if(computeInputInACoin(_inputValue)){
            m_state = READYTOSWITCHCURRENTPLAYER;
        }
    }

    if(hasAPlayerWon()){
        m_dataToDisplay._messages.push_back((std::string)"Congratulations Player " + std::to_string(_currentPlayer.getIdentifier()) + ", you won !");
        m_state = FINISHED;
    }
    else if(m_state == READYTOSWITCHCURRENTPLAYER){
        switchCurrentPlayer();
        m_state = STARTINGNEWTURN;
    }

    if(m_state == STARTINGNEWTURN){
        addTextInstructionsToDataToDisplay();
        m_state = AWAITINGPLACEORSELECTTOKEN;
    }

    m_dataToDisplay._grid =  getGridToDisplay();
    return m_dataToDisplay;

}


