//
// Created by Odyn on 11/22/2020.
//

#include <iostream>
#include <regex>
#include "Checkers.h"

Checkers::Checkers() : Game()
{
    initGame();
}



void Checkers::initGame()
{
    _p1._identifier = 1;
    _p2._identifier = 3;
    for(int i = 0; i < 4; i++)
    {
        if(i % 2 == 0)
            initGameEvenRow(i);
        else
            initGameOddRow(i);
    }
}

void Checkers::initGameOddRow(int i)
{
    for(int j = 0; j < 10; j++)
    {
        if(j % 2 != 0)
        {
            _grid.changeValueAt(i, j - 1, _p2.getIdentifier());
            _grid.changeValueAt(10-i-1, j, _p1.getIdentifier());
        }
    }
}

void Checkers::initGameEvenRow(int i)
{
    for(int j = 0; j < 10; j++)
    {
        if(j % 2 == 0)
        {
            _grid.changeValueAt(i, j + 1, _p2.getIdentifier());
            _grid.changeValueAt(10-i-1, j, _p1.getIdentifier());
        }
    }
}

std::string Checkers::readPlayerInput()
{
    std::string in;
    std::cout << "Player with id : " << _currentPlayer.getIdentifier() << ", please input the notation of the pawn you want to move. Format: a-j1-10 (for instance a5)" << std::endl;
    std::cin >> in;

    std::regex reg ("^[a-j]{1}[0-9]{1,2}$");

    if(std::regex_match(in, reg) && validateInput(in))
        return in;
    else
        return "";
}

bool Checkers::computeInputInACoin(const std::string& input)
{
    std::vector<std::string> choices = PawnHandler::giveChoices(input, _grid, _currentPlayer.getIdentifier());

    int i = 1;
    for(auto& choice : choices)
    {
        std::cout << i << ". " << choice << std::endl;
        i++;
    }
    if(choices.empty())
    {
        std::cout << "! This pawn doesn't have any available move !" << std::endl;
    }

    return choicesLoop(choices, i);

}

bool Checkers::choicesLoop(std::vector<std::string>& choices, int nbChoices)
{
    bool isOk = false;
    bool changed = false;
    std::string in;
    std::cout << "Enter a choice or 'q' to return to pawn selection" << std::endl;
    do{
        std::cout << "> ";

        std::cin >> in;
        std::cout << std::endl;

        int chosen;

        try
        {
            chosen = std::stoi(in);
        }
        catch (std::exception& e)
        {}

        if(chosen > 0 && chosen < nbChoices)
        {
            PawnHandler::computeChoice(choices[chosen-1], _grid, _currentPlayer.getIdentifier());
            isOk = true;
            changed = true;
            _grid.dameTransformation();
        }
        else if (in == "q")
        {
            isOk = true;
        }
        else
        {
            std::cout << "Enter a choice or 'q' to return to pawn selection" << std::endl;
        }
    }while(!isOk);

    return changed;
}

bool Checkers::validateInput(const std::string& input) const
{
    int row_number = std::stoi(input.substr(1));
    if(row_number > 10 || row_number < 1)
        return false;
    else
        return _grid.pawnAtNotationBelongsTo(input, _currentPlayer.getIdentifier());
}

bool Checkers::hasAPlayerWon() const
{
    return (_currentPlayer == _p1) ? (_grid.isAPlayerRemovedFromGrid(_p2.getIdentifier())): (_grid.isAPlayerRemovedFromGrid(_p1.getIdentifier()));
}

void Checkers::show()
{
    _grid.show();
}
