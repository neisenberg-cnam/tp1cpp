#ifndef CONSOLE_DISPLAY_H
#define CONSOLE_DISPLAY_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Display.h"

class ConsoleDisplay : public Display
{
public:

    ConsoleDisplay(Game& _gameToDisplay);

    void play() override;

    void input() override;

    void updateMessage(std::vector<std::string> _messages) override;

    void updateGrid(const std::vector<std::vector<std::string>>& _grid) override;

    void updateTokensList(const std::map<std::string, std::string>& _tokens) override;

};
#endif
