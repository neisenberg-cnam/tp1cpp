//
// Created by Odyn on 11/22/2020.
//

#ifndef TP4_PAWNHANDLER_H
#define TP4_PAWNHANDLER_H

#include <vector>
#include <string>
#include "DataStructures/GridCheckers.h"
#include "TreeNode.h"

class PawnHandler
{
public:
    PawnHandler();

    static std::vector<std::string> giveChoices(const std::string& position, GridCheckers& grid, int _player);
    static void computeChoice(const std::string& choice, GridCheckers& grid, int player);

private:
    static void
    createDecisionalTree(std::shared_ptr<TreeNode> _rootNode, GridCheckers &_grid, int _player, bool isDame);
    static void createChildrenFromNode(std::shared_ptr<TreeNode> _node, GridCheckers &_grid, int _player, bool isDame);
    static void tryAddPositionAsMoveChildNodeToNode(std::shared_ptr<TreeNode> _node, const std::string &_newPosition,
                                                    GridCheckers &_grid, int _player, bool isDame);
    static void
    tryAddPositionAsTakeChildNodeToNode(std::shared_ptr<TreeNode> _node, const std::string &_positionOfTaken,
                                        GridCheckers &_grid, int _player, bool isDame);

};


#endif //TP4_PAWNHANDLER_H
