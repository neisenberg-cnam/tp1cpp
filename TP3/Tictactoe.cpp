//
// Created by Odyn on 10/27/2020.
//

#include "Tictactoe.h"
#include <iostream>
#include <regex>

Tictactoe::Tictactoe() : Game(3, 3)
{
    initGame();
}

Tictactoe::Tictactoe(const Player &p1, const Player &p2) : Game(3, 3, p1, p2)
{
    initGame();
}

void Tictactoe::initGame()
{
    _currentPlayer = _p2;
}

//void Tictactoe::play()
//{
//    bool isGameOver = false;
//    while(!isGameOver)
//    {
//        _grid.show();
//        _currentPlayer = (_currentPlayer == _p1) ? _p2 : _p1;
//        playerPlayATurn();
//        if(hasAPlayerWon())
//        {
//            std::cout << "Congratulations Player " << ((_currentPlayer == _p1) ? "1" : "2") << ", you won !" << std::endl;
//            isGameOver = true;
//            _grid.show();
//        }
//        std::cout << "______________" << std::endl;
//    }
//}
//
//void Tictactoe::playerPlayATurn()
//{
//    bool valid = false;
//    while(!valid)
//    {
//        std::string raw_input = readPlayerInput();
//        if(!raw_input.empty())
//        {
//            valid = computeInputInACoin(raw_input);
//        }
//    }
//}

std::string Tictactoe::readPlayerInput()
{
    std::string in;
    std::cout << "Please input where you want to put your coin, format:\"x,y\" the top left is 0,0" << std::endl;
    std::cin >> in;

    std::regex reg ("^[0-9],[0-9]$");

    if(std::regex_match(in, reg))
        return in;
    else
        return "";
}

bool Tictactoe::computeInputInACoin(const std::string& input)
{
    std::string x_str = input.substr(0, input.find(','));
    std::string y_str = input.substr(input.find(',') + 1, input.length() - 1);
    int x = std::stoi(x_str);
    int y = std::stoi(y_str);
    if(x >= _grid.width() || y >= _grid.length() )
        return false;

    return _grid.changeValueAt(x, y, _currentPlayer.getIdentifier());
}

bool Tictactoe::hasAPlayerWon() const
{
    return (_grid.isAColumnFullOfValue(_currentPlayer.getIdentifier())
            || _grid.isARowFullOfValue(_currentPlayer.getIdentifier())
            || _grid.isADiagonalFullOfValue(_currentPlayer.getIdentifier()));
}