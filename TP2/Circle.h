//
// Created by Odyn on 10/13/2020.
//

#ifndef TP_CIRCLE_H
#define TP_CIRCLE_H
#ifndef PI_VALUE
#define PI_VALUE 3.13
#endif

#include "Point.h"
#include "util.h"

class Circle
{
public:
    Circle() : _diameter(0) {}

    Circle(Point p, int diameter) : _center(p), _diameter(diameter) {}

    inline Point &center() { return _center; }
    inline const Point &center() const { return _center; }

    inline int &diameter() { return _diameter; }
    inline const int &diameter() const { return _diameter; }

    inline double perimeter() const { return PI_VALUE * _diameter; }

    inline double area() const { return pow(_diameter / 2, 2) * PI_VALUE; }

    inline bool isOnCircle(Point &p) const { return floatEquals(p.distance(_center), _diameter / 2); }

    inline bool isIntoCircle(Point &p) const { return p.distance(_center) <= _diameter / 2; }


private:
    Point _center;
    int _diameter;
};

#endif //UNTITLED1_CIRCLE_H
