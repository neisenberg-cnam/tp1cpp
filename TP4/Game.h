//
// Created by Odyn on 10/27/2020.
//

#ifndef TP3_GAME_H
#define TP3_GAME_H

#include "Player.h"
#include "DataStructures/Grid.h"

class Game
{
public:
    Game();
    Game(const Player &p1, const Player &p2);
    void play();

protected:
    Player _p1;
    Player _p2;
    Player _currentPlayer;

//    Grid _grid;

    virtual void initGame() = 0;
    virtual void show() = 0;

    void playerPlayATurn();
    virtual std::string readPlayerInput() = 0;
    virtual bool computeInputInACoin(const std::string& input) = 0;
    virtual bool hasAPlayerWon() const = 0;
};

#endif //TP3_GAME_H
