//
// Created by Odyn on 10/27/2020.
//

#ifndef TP3_TICTACTOE_H
#define TP3_TICTACTOE_H

#include "../Player.h"
#include "../DataStructures/Grid.h"
#include "../Game.h"
#include <string>

class Tictactoe : public Game
{
public:
    Tictactoe();
    Tictactoe(const Player& p1, const Player& p2);

    inline std::string getGameType() const override { return "Tictactoe"; }

    inline const Grid& getGrid() const override { return _grid; }

private:
    void initGame() override;

    Grid _grid;

    std::vector<std::vector<std::string>> getGridToDisplay() override;

    std::string processPlayerInput(const std::string& _inputValue) override;
    bool computeInputInACoin(const std::string& input) override;
    bool hasAPlayerWon() const override;

    void addTextInstructionsToDataToDisplay() override;
};

#endif //TP3_TICTACTOE_H
