//
// Created by Odyn on 1/5/2021.
//

#ifndef TP5_QTUTILS_H
#define TP5_QTUTILS_H

#include <QtWidgets/QWidget>

namespace qt_utils
{
    struct Pos { int row = -1, col = -1; };

    Pos gridPosition(QWidget * widget);
}

#endif //TP5_QTUTILS_H
