//
// Created by user on 13/10/2020.
//

#ifndef TP_TRIANGLE_H
#define TP_TRIANGLE_H

#include <vector>
#include "Point.h"

class Triangle
{
public:
    // Default constructor
    Triangle() {}

    // Constructor with all parameters
    Triangle(Point a, Point b, Point c) : _a(a), _b(b), _c(c) {}

    // Accessors for the A vertex
    inline Point &aVertex() { return _a; }
    inline const Point &aVertex() const { return _a; }

    // Accessors for the B vertex
    inline Point &bVertex() { return _b; }
    inline const Point &bVertex() const { return _b; }

    // Accessors for the C vertex
    inline Point &cVertex() { return _c; }
    inline const Point &cVertex() const { return _c; }

    // Returns the base of the triangle defined by the longest side
    double base() const;

    // Returns the height of the triangle which is the length of the segment that comes from the opposite vertex and do a right angle with the base
    double height() const;

    // Returns the area of the triangle
    double area() const;

    // Returns a vector with the length of every side
    std::vector<double> lengths() const;

    // Self-explanatory
    bool isIsoceles() const;

    // If it's a right angled triangle
    bool isRectangle() const;

    // Self-explanatory
    bool isEquilateral() const;

    // Display all the relevant information
    void Afficher() const;

private:
    Point _a, _b, _c;
};

#endif //TP_TRIANGLE_H
