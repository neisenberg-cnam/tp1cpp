//
// Created by Odyn on 1/5/2021.
//

#include "MainWindow.h"
#include "../OldGames/Tictactoe.h"
#include "QtGridDisplay.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    setCentralWidget(new QWidget(this));
    setMinimumSize(400, 400);
    createMenus();
}

MainWindow::~MainWindow()
{
    if(currentGame)
    {
        free(currentGame);
    }
}


void MainWindow::createMenus()
{
    addMenuSaves();
    addMenuGame();
}

void MainWindow::addMenuGame()
{
    auto gamesMenu = menuBar()->addMenu(tr("&Games"));

    auto tictac = new QAction(tr("&Tictactoe"), this);
    connect(tictac, &QAction::triggered, this, &MainWindow::launchTicTacToe);
    gamesMenu->addAction(tictac);

    auto connectF = new QAction(tr("Connect&Four"), this);
    connect(connectF, &QAction::triggered, this, &MainWindow::launchConnectFour);
    gamesMenu->addAction(connectF);

    auto checkers = new QAction(tr("&Checkers"), this);
    connect(checkers, &QAction::triggered, this, &MainWindow::launchCheckers);
    gamesMenu->addAction(checkers);

//    auto othello = new QAction("&Othello", this);
//    connect(othello, &QAction::triggered, this, &MainWindow::launchOthello);
//    gamesMenu->addAction(othello);
}

void MainWindow::addMenuSaves()
{
    auto savesMenu = menuBar()->addMenu("&Files");

    auto save = new QAction(tr("&Save"), this);
    connect(save, &QAction::triggered, this, &MainWindow::saveTheGame);
    savesMenu->addAction(save);

    auto load = new QAction(tr("&Load"), this);
    connect(load, &QAction::triggered, this, &MainWindow::loadAGame);
    savesMenu->addAction(load);
}

void MainWindow::launchTicTacToe()
{
    auto game = new Tictactoe();
    currentGame = game;
    auto display = new QFrame(this);
    display->setLayout(new QtGridDisplay(display, *currentGame));
    setCentralWidget(display);
}

void MainWindow::launchConnectFour()
{
    auto game = new ConnectFour();
    currentGame = game;
    auto display = new QFrame(this);
    display->setLayout(new QtGridDisplay(display, *currentGame));
    setCentralWidget(display);
}

void MainWindow::launchCheckers()
{
    auto game = new Checkers();
    currentGame = game;
    auto display = new QFrame(this);
    display->setLayout(new QtGridDisplay(display, *currentGame));
    setCentralWidget(display);
}

void MainWindow::saveTheGame()
{
    auto fileName = QFileDialog::getSaveFileName(this, tr("Open a game"), "", tr("Game files (*.game)"));
    SaveHandler::saveGrid(currentGame->getGrid(), *currentGame, fileName.toStdString());
}

void MainWindow::loadAGame()
{
    auto fileName = QFileDialog::getOpenFileName(this, tr("Open a game"), "", tr("Game files (*.game)"));
    currentGame = LoadHandler::LoadGameFromPath(fileName.toStdString());
    auto display = new QFrame(this);
    display->setLayout(new QtGridDisplay(display, *currentGame));
    setCentralWidget(display);
}

