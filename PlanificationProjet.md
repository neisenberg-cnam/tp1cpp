## Les grandes lignes:

Othello: Plus simple que les dames, plus court a realiser que l'interface. Probablement judicieux de commencer par confier son dev a Lea et/ou Ines pour leur permettre de 
prendre en main le projet de NE et NF et son architecture.

Interface: Inspection de la doc de Qt, choisir un pool de soltuion possible.

## Deroulement

Pour le 09/12/2020: 

- Au minimum: 

* proposer une structure UML (classes et methodes) pour l'implementation de l'Othello (repartir de Game, et eventuellement d'une specialisation de Grid a la 
maniere de GridCheckers) >> Ines et Lea, Ines s'est proposee de demarrer cette reflexion. Tenir l'ensemble de l'equipe au courant de l'avancee et des propositions avant de dev.
* proposer de même une structure UML pour l'implementation de l'interface, inspecter la doc Qt pour cela, faire des retours a l'equipe pour se concerter sur les choix.

- Au mieux:

* faire une reunion au cours du week end ou avant mercredi si l'UML de l'Othello/l'interface est fini plus tôt et commencer le dev. Dev d'un des deux termine avant mercredi = Mashallah++