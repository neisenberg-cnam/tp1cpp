//
// Created by Odyn on 11/22/2020.
//

#ifndef TP4_CHECKERS_H
#define TP4_CHECKERS_H

#include "Game.h"
#include "DataStructures/GridCheckers.h"
#include "PawnHandler.h"

class Checkers : public Game
{
public:
    Checkers();
     Checkers(const Player& p1, const Player& p2);

    void initGame() override;
    void initGameOddRow(int i);
    void initGameEvenRow(int i);
    std::string processPlayerInput(const std::string& _inputValue) override;
    bool computeInputInACoin(const std::string& input) override;
    bool hasAPlayerWon() const override;

    bool validateInput(const std::string& input) const;

    inline const Grid& getGrid() const override { return _grid; }

    inline std::string getGameType() const override { return "Checkers"; }
    std::vector<std::vector<std::string>> getGridToDisplay() override;




private:
    PawnHandler _handler;
    GridCheckers _grid;
    std::string m_currentSelectedSquareNotation;
    std::vector<std::string> m_currentSelectedSquareAvailableChoices;

    void addTextInstructionsToDataToDisplay() override;
};

#endif //TP4_CHECKERS_H
