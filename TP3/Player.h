//
// Created by user on 14/10/2020.
//

#ifndef TP3_PLAYER_H
#define TP3_PLAYER_H

#include <cstdlib>

class Player
{
public:
    Player() : _identifier(std::rand() % 10) {}
    explicit Player(int id) : _identifier(id) {}

    inline int getIdentifier() const
    { return _identifier; }

    inline friend bool operator== (const Player& lhs, const Player& rhs)
    { return lhs.getIdentifier() == rhs.getIdentifier(); }


private:
    int _identifier;
};

#endif //TP3_PLAYER_H
