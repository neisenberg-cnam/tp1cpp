//
// Created by Odyn on 11/22/2020.
//

#ifndef TP4_CHECKERS_H
#define TP4_CHECKERS_H

#include "Game.h"
#include "DataStructures/GridCheckers.h"
#include "PawnHandler.h"

class Checkers : public Game
{
public:
    Checkers();
    // Checkers(const Player& p1, const Player& p2);

    void initGame() override;
    void initGameOddRow(int i);
    void initGameEvenRow(int i);
    void show() override;
    std::string readPlayerInput() override;
    bool computeInputInACoin(const std::string& input) override;
    bool hasAPlayerWon() const override;

    bool validateInput(const std::string& input) const;
    bool choicesLoop(std::vector<std::string>& choices, int nbChoices);



private:
    PawnHandler _handler;
    GridCheckers _grid;
};

#endif //TP4_CHECKERS_H
