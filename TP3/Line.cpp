//
// Created by user on 14/10/2020.
//

#include "Line.h"

Line::Line() : _size(3)
{}

Line::Line(int size) : _size(size)
{}

Line::Line(const Line &line) : _size(line._size)
{}

bool Line::setValueAtIndex(int value, int index)
{
    if(isSquareEmptyAtIndex(index))
    {
        _vec[index]->value = value;
        return true;
    }
    else
        return false;

}

bool Line::isSquareEmptyAtIndex(int index) const
{
    return _vec[index]->isEmpty();
}

int Line::getValueAtIndex(int index) const
{
    return _vec[index]->value;
}

void Line::setSquareAtIndex(const std::shared_ptr<Square> &squ, int index)
{
    _vec[index] = squ;
}

void Line::pushBackSquare(const std::shared_ptr<Square>& squ)
{
    _vec.push_back(squ);
}

std::shared_ptr<Square> Line::getSquareAtIndex(int index) const
{
    return _vec[index];
}

bool Line::isLineFull() const
{
    bool flag = true;
    for(auto const &square : _vec)
    {
        if (square->isEmpty())
            flag = false;
    }
    return flag;
}

bool Line::isLineFullOfValue(int value) const
{
    bool flag = true;
    for(auto const &square : _vec)
    {
        if (square->value != value)
            flag = false;
    }
    return flag;
}

std::shared_ptr<Square> Line::getLastEmptySquare() const
{
    for(int i = 0; i < _size; i++)
    {
        if(_vec[_size - i - 1]->isEmpty())
            return _vec[_size - i - 1];
    }
}