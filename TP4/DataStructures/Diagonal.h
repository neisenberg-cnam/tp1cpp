//
// Created by Odyn on 10/27/2020.
//

#ifndef TP3_DIAGONAL_H
#define TP3_DIAGONAL_H

#include "Line.h"

class Diagonal : public Line
{
public:
    Diagonal();
    Diagonal(int size);
    Diagonal(const Diagonal& dia);
};

#endif //TP3_DIAGONAL_H
