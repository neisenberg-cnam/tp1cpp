//
// Created by Odyn on 10/27/2020.
//

#ifndef TP3_TICTACTOE_H
#define TP3_TICTACTOE_H

#include "Player.h"
#include "Grid.h"
#include "Game.h"
#include <string>

class Tictactoe : public Game
{
public:
    Tictactoe();
    Tictactoe(const Player& p1, const Player& p2);

private:
    void initGame() override;

    std::string readPlayerInput() override;
    bool computeInputInACoin(const std::string& input) override;
    bool hasAPlayerWon() const override;
};

#endif //TP3_TICTACTOE_H
