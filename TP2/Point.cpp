
#include <cmath>
#include "Point.h"

double Point::distance(const Point &p) const
{
    // Distance to another point is the square root of the sum of the distance of
    // x at the power of 2 and the distance of y at the power of 2
    return std::sqrt(std::pow((x - p.x), 2) + std::pow((y - p.y), 2));
}

