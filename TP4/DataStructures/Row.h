//
// Created by Odyn on 10/27/2020.
//

#ifndef TP3_ROW_H
#define TP3_ROW_H

#include "Line.h"

class Row : public Line
{
public:
    Row();
    Row(int size);
    Row(const Row& row);
};

#endif //TP3_ROW_H
