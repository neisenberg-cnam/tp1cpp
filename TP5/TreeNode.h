#ifndef TREE_NODE_H
#define TREE_NODE_H

#include <string>
#include <vector>
#include <memory>
#include <list>
#include <iostream>
#include <assert.h>
#include <map>

class TreeNode
{
public:
    TreeNode(const std::string _position, std::string _pathToNode, bool _isTerminal);

    void addChildMove(std::string _position);

    void addChildTake(const std::string& _positionOfTaken, const std::string& _position);

    std::string getPosition() const;

    std::string getPathToThisNode() const;

    std::vector<std::shared_ptr<TreeNode>> getChildrenNode() const;

    bool isNodeARoot() const;

    static std::vector<std::shared_ptr<TreeNode>> findDeepestNodes(const std::shared_ptr<TreeNode>& _rootNode);

    bool m_isTerminal;

private:
    std::string m_position;

    std::string m_pathToThisNode;

    std::vector<std::shared_ptr<TreeNode>> m_childrenNode;

    static std::map<int, std::vector<std::shared_ptr<TreeNode>>> m_terminalNodesGroupedByDeepness;

    void findTerminalNodesAmongstChildrens(int _currentNodeDeepness) const;
};
#endif
