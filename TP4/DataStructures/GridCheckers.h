#pragma clang diagnostic push
#pragma ide diagnostic ignored "modernize-use-nodiscard"
//
// Created by Odyn on 11/22/2020.
//

#ifndef TP4_GRIDCHECKERS_H
#define TP4_GRIDCHECKERS_H

#include "Grid.h"

class GridCheckers : public Grid
{
public:
    GridCheckers();
//    GridCheckers(int width, int length);

    static int notationToIndex(const std::string& notation);
    static std::string coordinatesToNotation(int i , int j);

    bool isPawnAtNotationADameFromPlayer(const std::string& notation, int player) const;
    bool pathToNotationIsEmpty(std::string notationFrom, std::string notationTo) const;
    bool pawnAtNotationBelongsTo(const std::string& notation, int player) const;

    bool canNotationMoveAtNotation(const std::string& notationFrom, const std::string& notationTo, int current_player) const;
    bool canNotationTakeAtNotation(const std::string& notationFrom, const std::string& notationTo, int current_player) const;
    bool canPawnAtNotationMoveAtNotation(const std::string &notationFrom, const std::string &notationTo,
                                         int current_player) const;
    bool canDameAtNotationMoveAtNotation(const std::string &notationFrom, const std::string &notationTo,
                                         int current_player) const;
    bool canPawnAtNotationTakePawnAtNotation(const std::string &notationFrom, const std::string &notationTo,
                                             int current_player) const;
    bool canDameAtNotationTakePawnAtNotation(const std::string &notationFrom, const std::string &notationTo,
                                             int player) const;

    std::shared_ptr<Square> notationToSquare(const std::string& notation) const;
    static bool isAtRangeForPawn(const std::string &notationFrom, const std::string &notationTo, int current_player);
    bool isPawnGoingForward(const std::string &notationFrom, const std::string &notationTo, int current_player) const;
    bool isAtRangeForDame(const std::string &notationFrom, const std::string &notationTo) const ;
    std::string squareAfter(const std::string &notationFrom, const std::string &notationTo) const;
    bool isSquareAfterPosIsEmpty(const std::string &notationFrom, const std::string &notationTo) const;
    bool isOtherPawnOpponents(const std::string &notationFrom, const std::string &notationTo, int current_player) const ;

    std::vector<std::string> givePossibleNotationsAfterTake(const std::string& notationFrom, const std::string& notationTaken, int current_player) const;

    void dameTransformation();
    bool isAPlayerRemovedFromGrid(int player) const;

    void show() const override;


private:

};


#endif //TP4_GRIDCHECKERS_H

#pragma clang diagnostic pop
