//
// Created by Odyn on 10/27/2020.
//

#ifndef TP3_GAME_H
#define TP3_GAME_H

#include "Player.h"
#include "DataStructures/Grid.h"
#include "GameState.h"
#include "DataToDisplay.h"

class Game
{
public:
    Game();
    Game(const Player &p1, const Player &p2);
    inline int getPlayerOneIdentifier() const { return _p1.getIdentifier(); }
    inline int getPlayerTwoIdentifier() const { return _p2.getIdentifier(); }
    inline int getCurrentPlayerIdentifier() const { return _currentPlayer.getIdentifier(); }
    virtual std::string getGameType() const = 0;
    void switchCurrentPlayer();
    void play();

    // TODO: Refactor that Grid thing into another GridGame interface
    virtual const Grid& getGrid() const = 0;


    DataToDisplay& handleInput(std::string _inputValue);
    GameState getGameState() const;

protected:
    Player _p1;
    Player _p2;
    Player _currentPlayer;
    GameState m_state;
    DataToDisplay m_dataToDisplay;

    virtual void initGame() = 0;
    virtual std::vector<std::vector<std::string>> getGridToDisplay() = 0;

    void currentPlayerPlayATurn();
    virtual std::string processPlayerInput(const std::string& _inputValue) = 0;
    virtual bool computeInputInACoin(const std::string& input) = 0;
    virtual bool hasAPlayerWon() const = 0;
    virtual void addTextInstructionsToDataToDisplay() =0;

};

#endif //TP3_GAME_H
