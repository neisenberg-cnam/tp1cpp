//
// Created by Odyn on 12/9/2020.
//

#ifndef TP4_SAVEHANDLER_H
#define TP4_SAVEHANDLER_H


#include "DataStructures/Grid.h"
#include "Game.h"
#include "OldGames/ConnectFour.h"
#include "OldGames/Tictactoe.h"
#include "Checkers.h"
#include <fstream>

class SaveHandler
{
public:
    static void saveGrid(const Grid &grid, const Game &game, const std::string &path);
    static void saveTicTacToe(const Tictactoe &ttt, const std::string &path);
    static void saveConnectFour(const ConnectFour &connectFour, const std::string &path);
    static void saveCheckers(const Checkers &checkers, const std::string &path);
//    static void saveOthello(const Othello& othello);

    static void putGridInStream(std::ofstream& stream, const Grid &grid);

};


#endif //TP4_SAVEHANDLER_H
