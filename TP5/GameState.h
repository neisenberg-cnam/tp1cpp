#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

enum GameState
{
    STARTINGNEWTURN,
    AWAITINGPLACEORSELECTTOKEN,
    AWAITINGCHOOSEACTION,
    READYTOSWITCHCURRENTPLAYER,
    FINISHED
};

#endif // GAMESTATE_H
