#include <iostream>
#include <assert.h>
#include "DataStructures/Grid.h"
#include "OldGames/Tictactoe.h"
#include "OldGames/ConnectFour.h"
#include "Checkers.h"
#include <memory>
#include "TreeNode.h"

int main()
{
    std::string in;
    do
    {
        std::cout << "Type 1 for Tictactoe 2 for ConnectFour 3 for Checkers" << std::endl;
        std::cin >> in;
    } while(std::stoi(in) != 1 && std::stoi(in) != 2 && std::stoi(in) != 3);
    if(std::stoi(in) == 1)
    {
        auto game = Tictactoe();
        game.play();
    }
    else if (std::stoi(in) == 2)
    {
        auto game = ConnectFour();
        game.play();
    }
    else
    {
        auto game = Checkers();
        game.play();
    }
    return 0;
}

void TestTree()
{
    auto root = std::make_shared<TreeNode>("imroot", "", false);

    // First level
    root->addChildMove("child_of_root1");
    root->addChildTake("i", "child_of_root2");
    root->addChildTake("i", "child_of_root3");

    // Second level
    root->getChildrenNode()[1]->addChildTake("i", "child_of_second_children");
    root->getChildrenNode()[2]->addChildTake("i", "child_of_third_children");

    // Third level
    root->getChildrenNode()[1]->getChildrenNode()[0]->addChildTake("i", "child_of_first_grandchildren");
    root->getChildrenNode()[2]->getChildrenNode()[0]->addChildTake("i", "child_of_second_grandchildren");


    root->getChildrenNode()[1]->getChildrenNode()[0]->getChildrenNode()[0]->m_isTerminal = true;
    root->getChildrenNode()[2]->getChildrenNode()[0]->getChildrenNode()[0]->m_isTerminal = true;



    assert(TreeNode::findDeepestNodes(root).size() == 2);
}
