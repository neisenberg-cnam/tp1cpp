//
// Created by Odyn on 10/13/2020.
//

#include <cmath>
#include <algorithm>

#ifndef FLOAT_EPSILON
#define FLOAT_EPSILON 0.0001f
#endif

bool floatEquals(const double& a, const double& b)
{
    double x = 1.0f;
    double y = fabs(a);
    double z = fabs(b);
    // Computing a max between 0.0 and a and b to stay relevant if it's a little number or if it's a big number
    double max = std::max({x, y ,z});
    return (fabs(a - b) <= (FLOAT_EPSILON * max));
}