//
// Created by Odyn on 10/27/2020.
//

#include <iostream>
#include "Game.h"

Game::Game(int width, int length) : _p1(Player()), _p2(Player()), _grid(Grid(width,length))
{}

Game::Game(int width, int length, const Player &p1, const Player &p2) : _p1(p1), _p2(p2), _grid(Grid(width,length))
{}

void Game::play()
{
    bool isGameOver = false;
    while(!isGameOver)
    {
        _grid.show();
        _currentPlayer = (_currentPlayer == _p1) ? _p2 : _p1;
        playerPlayATurn();
        if(hasAPlayerWon())
        {
            std::cout << "Congratulations Player " << ((_currentPlayer == _p1) ? "1" : "2") << ", you won !" << std::endl;
            isGameOver = true;
            _grid.show();
        }
        std::cout << "______________" << std::endl;
    }
}

void Game::playerPlayATurn()
{
    bool valid = false;
    while(!valid)
    {
        std::string raw_input = readPlayerInput();
        if(!raw_input.empty())
        {
            valid = computeInputInACoin(raw_input);
        }
    }
}