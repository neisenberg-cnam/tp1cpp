//
// Created by user on 16/09/2020.
//

#include <string>
#include <algorithm>
#include <iostream>
#include <chrono>
#include <thread>
#include <cmath>

#define MAX_RANGE 1000
#define MIN_RANGE 1

void stringFirstUp(std::string &str)
{
    for(auto it = str.begin(); it != str.end(); it++)
    {
        if (it == str.begin())
        {
            *it = toupper(*it);
        }
        else
        {
            *it = tolower(*it);
        }
    }
}

void greetings() {
    std::cout << "Please input your first name and last name" << std::endl;
    std::string input;
    std::getline(std::cin, input);

    auto delim = input.find(' ');

    // delim++ here to put delim after the space for the second substr.
    std::string fname = input.substr(0, delim++);
    std::string lname = input.substr(delim, input.length() - delim);

    stringFirstUp(fname);
    std::transform(lname.begin(), lname.end(), lname.begin(), ::toupper);

    std::cout << "Greetings " << fname << " " << lname << std::endl;
}

void humanVsNb()
{
    int target = rand() % MAX_RANGE;
    std::cout << "The computer chosed a number, please input your first guess" << std::endl;

    int guess;
    std::cin >> guess;

    while (guess > MAX_RANGE || guess < MIN_RANGE)
    {
        guess = -1;
        std::cout << "Please enter a number between " << MIN_RANGE << " and " << MAX_RANGE << std::endl;
        std::cin >> guess;
    }

    int tries = 1;
    bool win = false;
    while(!win)
    {
        if (guess == target)
        {
            std::cout << "Congratulations, you guessed the right number in " << tries << " ";
            (tries == 1) ? (std::cout << "try") : (std::cout << "tries");
            std::cout << std::endl;
            win = true;
        }
        else if (guess > target)
        {
            std::cout << "The number is lower than your guess, please enter another one" << std::endl;
            std::cin >> guess;
        }
        else if (guess < target)
        {
            std::cout << "The number is greater than your guess, please enter another one" << std::endl;
            std::cin >> guess;
        }
        tries++;
    }
}

int middle(int max, int min)
{
    return std::floor((max + min) / 2);
}

void cpuVsNum()
{
    std::cout << "Please choose a number, the computer will soon try to guess. You can type 'h' when prompted to get instructions" << std::endl;

    std::this_thread::sleep_for(std::chrono::seconds(3));

    bool win = false;
    bool played = true;
    int min = MIN_RANGE;
    int max = MAX_RANGE;
    int guess;

    while (!win)
    {
        if (played) {
            guess = middle(max, min);
            std::cout << "Guess: " << guess << std::endl;
            if (min == max == guess)
            {
                std::cout << "Either you changed the number or you tried to cheat. Game ending." << std::endl;
                win = true;
            }
        }

        char in;
        std::cin >> in;

        if (in == '=')
        {
            std::cout << "Game ending" << std::endl;
            win = true;
        }
        else if (in == '>')
        {
            min = guess;
            played = true;
        }
        else if (in == '<')
        {
            max = guess;
            played = true;
        }
        else if (in == 'h')
        {
            std::cout << "Avalaible options:" << std::endl
                    << "\t> : indicate that your number is greater than the guess" << std::endl
                    << "\t< : indicate that your number is lower than the guess" << std::endl
                    << "\t= : indicate that the guess was right" << std::endl
                    << "\th : prompts the avalaible options" << std::endl;
            played = false;
        }
        else
        {
            std::cout << "Please enter one of the avalaible options. Enter 'h' for more instructions" << std::endl;
            played = false;
        }
    }
}

void guessGame()
{
    std::cout << "Type 1 to guess the number, type 2 for the CPU to guess the number." << std::endl;
    int in;
    std::cin >> in;

    while(in != 2 && in != 1)
    {
        std::cout << "Type 1 to guess the number, type 2 for the CPU to guess the number." << std::endl;
        std::cin >> in;
    }

    if (in == 1)
    {
        humanVsNb();
        return;
    }
    else if (in == 2)
    {
        cpuVsNum();
        return;
    }
}