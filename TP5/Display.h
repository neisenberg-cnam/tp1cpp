#ifndef I_DISPLAY_H
#define I_DISPLAY_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>
#include <map>

#include "Game.h"
#include "GameState.h"
#include "SaveHandler.h"
#include "LoadHandler.h"

class Display
{
protected:
    std::map<std::string, std::string> m_tokensList;
    Game& m_gameDisplayed;


public:
    Display(Game& _gameToDisplay);

    virtual void play()=0;

    virtual void input()=0;

    virtual void updateGrid(const std::vector<std::vector<std::string>>& _grid)=0;

    virtual void updateMessage(std::vector<std::string> _messages)=0;

    virtual void updateTokensList(const std::map<std::string, std::string>& _tokens)=0;

};
#endif
