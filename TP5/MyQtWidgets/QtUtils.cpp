//
// Created by Odyn on 1/5/2021.
//

#include <QtWidgets/QGridLayout>
#include "QtUtils.h"

qt_utils::Pos qt_utils::gridPosition(QWidget *widget)
{
    if (! widget->parentWidget()) return {};
    auto layout = qobject_cast<QGridLayout*>(widget->parentWidget()->layout());
    if (! layout) return {};
    int index = layout->indexOf(widget);
    Q_ASSERT(index >= 0);
    int _;
    Pos pos;
    layout->getItemPosition(index, &pos.row, &pos.col, &_, &_);
    return pos;
}