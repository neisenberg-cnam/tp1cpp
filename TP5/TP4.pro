TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    Checkers.cpp \
    ConsoleDisplay.cpp \
    DataStructures/Column.cpp \
    DataStructures/Diagonal.cpp \
    DataStructures/Grid.cpp \
    DataStructures/GridCheckers.cpp \
    DataStructures/Line.cpp \
    DataStructures/Row.cpp \
    DataStructures/Square.cpp \
    Display.cpp \
    Game.cpp \
    LoadHandler.cpp \
    OldGames/ConnectFour.cpp \
    OldGames/Tictactoe.cpp \
    PawnHandler.cpp \
    SaveHandler.cpp \
    TreeNode.cpp \
    main.cpp

HEADERS += \
    Checkers.h \
    ConsoleDisplay.h \
    DataStructures/Column.h \
    DataStructures/Diagonal.h \
    DataStructures/Grid.h \
    DataStructures/GridCheckers.h \
    DataStructures/Line.h \
    DataStructures/Row.h \
    DataStructures/Square.h \
    DataToDisplay.h \
    Display.h \
    Game.h \
    GameState.h \
    LoadHandler.h \
    OldGames/ConnectFour.h \
    OldGames/Tictactoe.h \
    PawnHandler.h \
    Player.h \
    SaveHandler.h \
    TreeNode.h
